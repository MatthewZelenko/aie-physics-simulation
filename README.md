![Banner](https://bytebucket.org/MatthewZelenko/opengl-physics/raw/281b0607dcaa16ac11fb970fde5c04c7f9eaf36f/Physics%20Banner.jpg)

###OpenGL Physics Project###
This was created using Visual Studio 2015.

Summary:
This is a demo with four scenes displaying collision, springs, rockets, and predictions.
The Controls are inside. 

Requirements:
Download, open in Visual Studio, run.
Alternatively you can download the build [here](https://drive.google.com/uc?export=download&id=0BzmWDCip_PJLVlItdG5GM0plWkE)