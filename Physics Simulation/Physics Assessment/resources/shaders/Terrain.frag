#version 330 core

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

uniform vec3 cameraPos = vec3(0.0f);
uniform vec3 lightPos = vec3(0.0f);
uniform vec3 lightColour = vec3(1.0f);
uniform float amplitude = 1.0f;

uniform float specularStr = 0.05f;
uniform float ambientStr = 0.3f;

void main()
{
	vec4 diffColour = vec4(1.0f);

	//ambient
	vec3 ambient = ambientStr * lightColour;

	//diffuse
	vec3 norm = normalize(vNormal);
	vec3 lightDir = normalize(lightPos - vPosition);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * lightColour;

	//specular
	vec3 viewDir = normalize(cameraPos - vPosition);
	vec3 reflectDir = reflect(-lightDir, norm);
	float viewDirDot = max(0.0, dot(viewDir, reflectDir));
	float spec = pow(viewDirDot, 32);
	vec3 specular = spec * specularStr * lightColour;

	gl_FragColor = vec4(ambient + diffuse + specular, 1.0f) * diffColour;
}