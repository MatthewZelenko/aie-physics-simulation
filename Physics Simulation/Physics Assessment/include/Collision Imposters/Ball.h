#pragma once
#include "GameObject.h"
class Ball : public GameObject
{
public:
	Ball(std::string a_name, glm::vec3& a_position, glm::vec3& a_size);
	~Ball();

	void ProcessInput(float a_deltaTime);
	void Update(float a_deltaTime);

	void OnCollisionEnter(GameObject* a_collider);
	void OnCollisionStay(GameObject* a_collider);
	void OnCollisionExit(GameObject* a_collider);

private:

};