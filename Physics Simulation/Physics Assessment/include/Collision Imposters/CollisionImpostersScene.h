#pragma once
#include "Scene.h"
#include "Terrain.h"

class Application;

class CollisionImpostersScene : public Scene
{
public:
	CollisionImpostersScene(Application* a_application);
	~CollisionImpostersScene();

	void Load();
	void ProcessInput(float a_deltaTime);
	void Update(float a_deltaTime);
	void Render(float a_deltaTime);
	void UnLoad();

private:
	Terrain m_Terrain;
	Shader m_TerrainShader;
};