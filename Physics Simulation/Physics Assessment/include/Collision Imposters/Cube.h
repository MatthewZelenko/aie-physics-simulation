#pragma once
#include "GameObject.h"
class Cube : public GameObject
{
public:
	Cube(std::string a_name, glm::vec3& a_position, glm::vec3& a_size);
	~Cube();

	void ProcessInput(float a_deltaTime);
	void Update(float a_deltaTime);

	void OnCollisionEnter(GameObject* a_collider);
	void OnCollisionStay(GameObject* a_collider);
	void OnCollisionExit(GameObject* a_collider);

private:

};