#pragma once
#include "Application.h"
#include "FPSCamera.h"
#include "Terrain.h"
#include "Debugger.h"

class PhysicsAssessment :
	public Application
{
public:
	PhysicsAssessment(std::string a_gameTitle, unsigned int a_gameWidth, unsigned int a_gameHeight);
	~PhysicsAssessment();

	void Load();
	void ProcessInput();
	void Update();
	void Render();
	void UnLoad();

protected:

};