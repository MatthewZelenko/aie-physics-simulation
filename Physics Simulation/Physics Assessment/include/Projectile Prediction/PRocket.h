#pragma once
#include "GameObject.h"
class PRocket : public GameObject
{
public:
	PRocket(std::string a_name, glm::vec3& a_position, glm::vec3& a_size);
	~PRocket();

	void ProcessInput(float a_deltaTime);
	void Update(float a_deltaTime);
private:

};