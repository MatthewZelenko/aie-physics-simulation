#pragma once
#include "Scene.h"
#include "Terrain.h"

class Application;

class ProjectilePredictionScene : public Scene
{
public:
	ProjectilePredictionScene(Application* a_application);
	~ProjectilePredictionScene();

	void Load();
	void ProcessInput(float a_deltaTime);
	void Update(float a_deltaTime);
	void Render(float a_deltaTime);
	void UnLoad();

private:
	Terrain m_Terrain;
	Shader m_TerrainShader;
};