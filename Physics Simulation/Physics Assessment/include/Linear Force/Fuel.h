#pragma once
#include "GameObject.h"

class Fuel : public GameObject
{
public:
	Fuel(float a_fuel);
	~Fuel();

	void Update(float a_deltaTime);

	float GetFuel() { return m_CurrentFuel; }

private:
	float m_MaxFuel;
	float m_CurrentFuel;
	static int m_Num;
};