#pragma once
#include "GameObject.h"
class Rocket : public GameObject
{
public:
	Rocket(std::string a_name, glm::vec3& a_position, glm::vec3& a_size, float a_fuel);
	~Rocket();

	void ProcessInput(float a_deltaTime);
	void Update(float a_deltaTime);

	float TakeFuel(float a_fuel);

	float GetFuel() { return m_Fuel; }
	glm::vec3 GetForward() { return m_Forward; }

private:
	float m_Fuel;
	glm::vec3 m_Forward;
};