#pragma once
#include "Scene.h"
#include "Terrain.h"

class Application;

class LinearForceScene : public Scene
{
public:
	LinearForceScene(Application* a_application);
	~LinearForceScene();

	void Load();
	void ProcessInput(float a_deltaTime);
	void Update(float a_deltaTime);
	void FixedUpdate(float a_timeStep);
	void Render(float a_deltaTime);
	void UnLoad();

private:
	Terrain m_Terrain;
	Shader m_TerrainShader;
	bool m_rocketIsActive;
};