#pragma once
#include "Scene.h"
#include "Terrain.h"

class Application;

class JointsScene : public Scene
{
public:
	JointsScene(Application* a_application);
	~JointsScene();

	void Load();
	void ProcessInput(float a_deltaTime);
	void Update(float a_deltaTime);
	void Render(float a_deltaTime);
	void UnLoad();

private:
	Terrain m_Terrain;
	Shader m_TerrainShader;
};