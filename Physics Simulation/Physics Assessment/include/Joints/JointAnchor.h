#pragma once
#include "GameObject.h"
class JointAnchor : public GameObject
{
public:
	JointAnchor(std::string a_name, glm::vec3& a_position, glm::vec3& a_size);
	~JointAnchor();

	void ProcessInput(float a_deltaTime);
	void Update(float a_deltaTime);

private:

};