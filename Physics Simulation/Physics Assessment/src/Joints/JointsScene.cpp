#include "Joints\JointScene.h"
#include "Application.h"
#include <Input.h>
#include "TrackCamera.h"
#include "PhysicsUtilities.h"
#include "Joints\JointAnchor.h"

JointsScene::JointsScene(Application* a_application) : Scene(a_application)
{

}
JointsScene::~JointsScene()
{

}

void JointsScene::Load()
{
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//Camera
	TrackCamera* trackCamera = new TrackCamera(&m_Application->GetWindow(), nullptr, glm::vec3(0, 75, 150));
	trackCamera->SetPerspective(45.0f, (float)m_Application->GetWidth() / (float)m_Application->GetHeight(), 0.1f, 10000.0f);
	AddCamera(trackCamera);

	//Debugger
	m_Debugger.Init(m_CurrentCamera->GetProjection());
	m_Debugger.CreateGrid(10, 10, glm::vec4(0.2f, 0.5f, 1.0f, 1.0f));

	//Terrain Shader
	m_TerrainShader.CreateFromFile("resources/shaders/", "Terrain.vert", nullptr, "Terrain.frag");
	m_TerrainShader.Use();
	m_TerrainShader.SendMat4("projection", 1, m_CurrentCamera->GetProjection());
	m_TerrainShader.SendMat4("view", 1, m_CurrentCamera->GetView());

	//Terrain
	m_Terrain.SetLandAmplitude(200.0f);
	m_Terrain.SetLandFrequency(0.005f);
	m_Terrain.SetLandPersistence(0.2f);
	m_Terrain.SetLandOctave(8);
	m_Terrain.SetLandSeed(2481);
	m_Terrain.Init(400, 400, 10.0f, 10.0f);

	JointAnchor* anchor = new JointAnchor("Anchor", glm::vec3(0, 100, 0), glm::vec3(4));
	SphereCollider& col = anchor->CreateComponent<SphereCollider>("Collider");
	m_CollisionManager.Register(&col);
	AddGameObject(anchor->GetName(), anchor);
	
	trackCamera->SetTrackingObject(anchor);
	
	GameObject* prev = anchor;
	
	for (int i = 0; i < 20; ++i)
	{
		GameObject* obj = new GameObject("Obj" + std::to_string(i), glm::vec3(10 + (i * 10), 100, 0), glm::vec3(2));
		RigidBody& rb = obj->CreateComponent<RigidBody>("RigidBody");
		rb.SetMass(1);
		SphereCollider& col2 = obj->CreateComponent<SphereCollider>("Collider");
		col2.SetRestitution(0.2f);
		m_CollisionManager.Register(&col2);
		AddGameObject(obj->GetName(), obj);
	
		SpringJoint& spring = prev->CreateComponent<SpringJoint>("Spring");
		spring.SetEndPoint(&rb);
		prev = obj;
	}

	//int index = 0;
	//for (int z = 0; z < 5; ++z)
	//{
	//	for (int x = 0; x < 5; ++x)
	//	{
	//		if (z == 0 || x == 0 || z == 4 || x == 4)
	//		{
	//			JointAnchor* anchor = new JointAnchor("Anchor" + std::to_string(index), glm::vec3(x * 10, 40, z * 10), glm::vec3(4));
	//			if (index == 0)
	//			{
	//				trackCamera->SetTrackingObject(anchor);
	//			}
	//			Transform* trans = anchor->GetComponent<Transform>();
	//			trans->SetDebugType(DebugType::CUBE);
	//			AABBCollider& col = anchor->CreateComponent<AABBCollider>("Collider");
	//			col.SetRestitution(0.4f);
	//			m_CollisionManager.Register(&col);
	//			AddGameObject(anchor->GetName(), anchor);
	//		}
	//		else
	//		{
	//			GameObject* obj = new GameObject("Obj" + std::to_string(index), glm::vec3(x * 10, 40, z * 10), glm::vec3(1));
	//			RigidBody& rb = obj->CreateComponent<RigidBody>("RigidBody");
	//			rb.SetMass(1);
	//			SphereCollider& col = obj->CreateComponent<SphereCollider>("Collider");
	//			col.SetRestitution(0.2f);
	//			m_CollisionManager.Register(&col);
	//			AddGameObject(obj->GetName(), obj);
	//		}
	//		index++;
	//	}
	//}
	//index = 0;
	//for (int z = 0; z < 5; ++z)
	//{
	//	for (int x = 0; x < 5; ++x)
	//	{
	//		if ((x + 1) % 2 == 0 && (z + 1) % 2 == 0)
	//		{
	//			GameObject* center = GetGameObject(index);
	//			RigidBody* rb = nullptr;
	//
	//			//X
	//			GameObject* negX = GetGameObject((z * 5) + (x - 1));
	//			rb = negX->GetComponent<RigidBody>();
	//			SpringJoint* s = &center->CreateComponent<SpringJoint>("spring1");
	//			s->SetEndPoint(rb);
	//
	//			if (x != 5)
	//			{
	//				GameObject* posX = GetGameObject((z * 5) + (x + 1));
	//				rb = posX->GetComponent<RigidBody>();
	//				s = &center->CreateComponent<SpringJoint>("spring2");
	//				s->SetEndPoint(rb);
	//			}
	//
	//			//Z
	//			GameObject* negZ = GetGameObject(((z - 1) * 5) + x);
	//			rb = negZ->GetComponent<RigidBody>();
	//			s = &center->CreateComponent<SpringJoint>("spring3");
	//			s->SetEndPoint(rb);
	//			if (z != 5)
	//			{
	//				GameObject* posZ = GetGameObject(((z + 1) * 5) + x);
	//				rb = posZ->GetComponent<RigidBody>();
	//				s = &center->CreateComponent<SpringJoint>("spring4");
	//				s->SetEndPoint(rb);
	//			}
	//
	//			//-X
	//			GameObject* negXNegZ = GetGameObject(((z - 1) * 5) + (x - 1));
	//			rb = negXNegZ->GetComponent<RigidBody>();
	//			s = &center->CreateComponent<SpringJoint>("spring5");
	//			s->SetEndPoint(rb);
	//			if (z != 5)
	//			{
	//				GameObject* negXPosZ = GetGameObject(((z + 1) * 5) + (x - 1));
	//				rb = negXPosZ->GetComponent<RigidBody>();
	//				s = &center->CreateComponent<SpringJoint>("spring6");
	//				s->SetEndPoint(rb);
	//			}
	//
	//			//+X
	//			GameObject* posXNegZ = GetGameObject(((z - 1) * 5) + (x + 1));
	//			rb = posXNegZ->GetComponent<RigidBody>();
	//			s = &center->CreateComponent<SpringJoint>("spring7");
	//			s->SetEndPoint(rb);
	//			if (x != 5)
	//			{
	//				GameObject* posXPosZ = GetGameObject(((z + 1) * 5) + (x + 1));
	//				rb = posXPosZ->GetComponent<RigidBody>();
	//				s = &center->CreateComponent<SpringJoint>("spring8");
	//				s->SetEndPoint(rb);
	//			}
	//		}
	//		index++;
	//	}
	//}
}
void JointsScene::ProcessInput(float a_deltaTime)
{
	if (Input::Get()->KeyPressed(GLFW_KEY_E))
	{
		NextCamera();
	}
	if (Input::Get()->KeyPressed(GLFW_KEY_Q))
	{
		PreviousCamera();
	}
	if (Input::Get()->KeyPressed(GLFW_KEY_G))
	{
		if (glm::length(GetGravity()) > 0)
			SetGravity(glm::vec3(0));
		else
			SetGravity(glm::vec3(0, -98, 0));
	}
	if (Input::Get()->KeyDown(GLFW_KEY_EQUAL))
	{
		m_Terrain.AddLandSeed(1);
		m_Terrain.GenerateLand();
	}if (Input::Get()->KeyDown(GLFW_KEY_MINUS))
	{
		m_Terrain.AddLandSeed(-1);
		m_Terrain.GenerateLand();
	}
}
void JointsScene::Update(float a_deltaTime)
{
	m_CurrentCamera->Update((float)m_Application->GetDeltaTime());
	for (auto& iter : m_GameObjects)
	{
		GameObject* obj = iter.second;
		Transform* trans = obj->GetComponent<Transform>();
		RigidBody* rb = obj->GetComponent<RigidBody>();
		SphereCollider* sb = obj->GetComponent<SphereCollider>();
		if (sb)
		{
			glm::vec3 pos = trans->GetPosition();
			float h = m_Terrain.GetLandHeightFromPosition(pos.x, pos.z, true);
			float radius = sb->GetRadius();
			if (pos.y <= h + radius)
			{
				glm::vec3 collisionNormal = m_Terrain.GetLandNormalFromPosition(pos.x, pos.z, true);
				glm::vec3 collisionVector = collisionNormal * (glm::dot(rb->GetVelocity(), collisionNormal));
				glm::vec3 forceVector = collisionVector * 1.0f / (rb->GetInverseMass());
				float res1 = sb->GetRestitution();
				rb->ApplyForce(-forceVector - (forceVector * res1));

				trans->SetPosition(glm::vec3(pos.x, h + radius, pos.z));
			}
		}
		AABBCollider* ab = obj->GetComponent<AABBCollider>();
		if (ab)
		{
			glm::vec3 pos = trans->GetPosition();
			float h = m_Terrain.GetLandHeightFromPosition(pos.x, pos.z, true);
			float y = ab->GetSize().y * 0.5f;
			if (pos.y <= h + y)
			{
				glm::vec3 collisionNormal = m_Terrain.GetLandNormalFromPosition(pos.x, pos.z, true);
				glm::vec3 collisionVector = collisionNormal * (glm::dot(rb->GetVelocity(), collisionNormal));
				glm::vec3 forceVector = collisionVector * 1.0f / (rb->GetInverseMass());
				float res1 = ab->GetRestitution();
				rb->ApplyForce(-forceVector - (forceVector * res1));

				trans->SetPosition(glm::vec3(pos.x, h + y, pos.z));
			}
		}
	}
}
void JointsScene::Render(float a_deltaTime)
{
	//Light
	glm::vec3 lightPos = glm::vec3(100000, 50000, 0);
	glm::vec3 lightColour = glm::vec3(1.0f, 0.75f, 0.75f);

	//Terrain
	m_TerrainShader.Use();
	m_TerrainShader.SendMat4("view", 1, m_CurrentCamera->GetView());
	m_TerrainShader.SendVec3("cameraPos", 1, m_CurrentCamera->GetPosition());
	m_TerrainShader.SendVec3("lightPos", 1, lightPos);
	m_TerrainShader.SendVec3("lightColour", 1, lightColour);
	m_TerrainShader.SendFloat("specularStr", 0.25f);
	m_TerrainShader.SendFloat("ambientStr", 0.1f);
	m_Terrain.DrawLand(m_TerrainShader);

	//Debugger
	m_Debugger.Draw(m_CurrentCamera->GetView());
	m_Debugger.ClearObjects();
}
void JointsScene::UnLoad()
{
	m_TerrainShader.Destroy();
	m_Terrain.Destroy();
}
