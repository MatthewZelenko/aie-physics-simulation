#include "Joints\JointAnchor.h"
#include "Input.h"
#include "GLM\gtx\rotate_vector.hpp"
#include "GameObject.h"

JointAnchor::JointAnchor(std::string a_name, glm::vec3& a_position, glm::vec3& a_size) : GameObject(a_name, a_position, a_size)
{
	RigidBody& rb = CreateComponent<RigidBody>("RigidBody");
	rb.SetStatic();
}
JointAnchor::~JointAnchor()
{
}

void JointAnchor::ProcessInput(float a_deltaTime)
{
	if (Input::Get()->KeyDown(GLFW_KEY_D))
	{
		(*m_Transforms.begin()).second.AddPosition(glm::vec3(100 * a_deltaTime, 0, 0));
	}
	if (Input::Get()->KeyDown(GLFW_KEY_A))
	{
		(*m_Transforms.begin()).second.AddPosition(glm::vec3(-100 * a_deltaTime, 0, 0));
	}
	if (Input::Get()->KeyDown(GLFW_KEY_W))
	{
		(*m_Transforms.begin()).second.AddPosition(glm::vec3(0, 0, -100 * a_deltaTime));
	}
	if (Input::Get()->KeyDown(GLFW_KEY_S))
	{
		(*m_Transforms.begin()).second.AddPosition(glm::vec3(0, 0, 100 * a_deltaTime));
	}
}
void JointAnchor::Update(float a_deltaTime)
{

}