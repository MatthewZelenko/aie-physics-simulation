#include "Physics Assessment.h"
#include <GLM\gtc\matrix_transform.hpp>
#include <Input.h>
#include <time.h>
#include "Linear Force\LinearForceScene.h"
#include "Projectile Prediction\ProjectilePredictionScene.h"
#include "Collision Imposters\CollisionImpostersScene.h"
#include "Joints\JointScene.h"

PhysicsAssessment::PhysicsAssessment(std::string a_gameTitle, unsigned int a_gameWidth, unsigned int a_gameHeight) : Application(a_gameTitle, a_gameWidth, a_gameHeight)
{

}
PhysicsAssessment ::~PhysicsAssessment()
{
	
}

void PhysicsAssessment::Load()
{
	AddScene(new JointsScene(this), "JointsScene");
	AddScene(new CollisionImpostersScene(this), "CollisionImpostersScene");
	AddScene(new ProjectilePredictionScene(this), "ProjectilePredictionScene");
	AddScene(new LinearForceScene(this), "LinearForceScene");
}
void PhysicsAssessment::ProcessInput()
{
	if (Input::Get()->KeyPressed(GLFW_KEY_1))
	{
		ChangeScene("JointsScene");
	}
	if (Input::Get()->KeyPressed(GLFW_KEY_2))
	{
		ChangeScene("CollisionImpostersScene");
	}
	if (Input::Get()->KeyPressed(GLFW_KEY_3))
	{
		ChangeScene("ProjectilePredictionScene");
	}
	if (Input::Get()->KeyPressed(GLFW_KEY_4))
	{
		ChangeScene("LinearForceScene");		
	}
}
void PhysicsAssessment::Update()
{
	
}
void PhysicsAssessment::Render()
{
	
}
void PhysicsAssessment::UnLoad()
{
	
}