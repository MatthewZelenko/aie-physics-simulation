#include "Projectile Prediction\ProjectilePredictionScene.h"
#include "FlyCamera.h"
#include "Application.h"
#include <Input.h>
#include "Projectile Prediction\PRocket.h"
#include "TrackCamera.h"
#include "PhysicsUtilities.h"

ProjectilePredictionScene::ProjectilePredictionScene(Application* a_application) : Scene(a_application)
{
}
ProjectilePredictionScene::~ProjectilePredictionScene()
{

}

void ProjectilePredictionScene::Load()
{
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//Camera
	TrackCamera* camera = new TrackCamera(&m_Application->GetWindow(), nullptr, glm::vec3());
	camera->SetPerspective(45.0f, (float)m_Application->GetWidth() / (float)m_Application->GetHeight(), 0.1f, 10000.0f);
	AddCamera(camera);

	FlyCamera* flyCamera = new FlyCamera(&m_Application->GetWindow(), 200.0f, 0.002f);
	flyCamera->SetInputKeys(GLFW_KEY_W, GLFW_KEY_S, GLFW_KEY_A, GLFW_KEY_D, GLFW_MOUSE_BUTTON_2);
	flyCamera->SetPerspective(45.0f, (float)m_Application->GetWidth() / (float)m_Application->GetHeight(), 0.1f, 10000.0f);
	flyCamera->SetView(glm::vec3(250.f, 200.0f, 300.0f), glm::vec3(0.0f, -0.5f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	AddCamera(flyCamera);

	//Debugger
	m_Debugger.Init(m_CurrentCamera->GetProjection());
	m_Debugger.CreateGrid(10, 10, glm::vec4(0.2f, 0.5f, 1.0f, 1.0f));

	//Terrain Shader
	m_TerrainShader.CreateFromFile("resources/shaders/", "Terrain.vert", nullptr, "Terrain.frag");
	m_TerrainShader.Use();
	m_TerrainShader.SendMat4("projection", 1, m_CurrentCamera->GetProjection());
	m_TerrainShader.SendMat4("view", 1, m_CurrentCamera->GetView());

	//Terrain
	m_Terrain.SetLandAmplitude(200.0f);
	m_Terrain.SetLandFrequency(0.005f);
	m_Terrain.SetLandPersistence(0.2f);
	m_Terrain.SetLandOctave(8);
	m_Terrain.SetLandSeed(150);
	m_Terrain.Init(200, 200, 10.0f, 10.0f);

	PRocket* rocket = new PRocket("Rocket", glm::vec3(0, 4, 0), glm::vec3(4, 6, 4));
	Transform* trans = rocket->GetComponent<Transform>();
	trans->SetDebugColour(glm::vec4(0.0f, 0.25f, 0.75f, 1.0f));
	RigidBody& rb = rocket->CreateComponent<RigidBody>("RigidBody");
	rb.SetMass(4);
	rb.ApplyForce(glm::vec3(500.0f, 400.0f, 0.0f));
	AddGameObject("Rocket", rocket);
	camera->SetTrackingObject(rocket);
	camera->SetDisplacement(glm::vec3(0.0f, 100.0f, 200.0f));

	for (int i = 0; i < 1000; ++i)
	{
		glm::vec3 pos = PredictPosition(trans->GetPosition(), rb.GetVelocity(), GetGravity(), i * 0.1f);
		GameObject* p = new GameObject(std::to_string(i), pos, glm::vec3(1.0f));
		p->GetComponent<Transform>()->SetDebugColour(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
		AddGameObject(p->GetName(), p);
	}
}
void ProjectilePredictionScene::ProcessInput(float a_deltaTime)
{
	if (Input::Get()->KeyPressed(GLFW_KEY_E))
	{
		NextCamera();
	}
	if (Input::Get()->KeyPressed(GLFW_KEY_Q))
	{
		PreviousCamera();
	}
}
void ProjectilePredictionScene::Update(float a_deltaTime)
{
	m_CurrentCamera->Update((float)m_Application->GetDeltaTime());
	GameObject* rocket = GetGameObject("Rocket");
	Transform* trans = rocket->GetComponent<Transform>();
	RigidBody* rb = rocket->GetComponent<RigidBody>();
	glm::vec3 pos = trans->GetPosition();
	float h = m_Terrain.GetLandHeightFromPosition(pos.x, pos.z, true) + (trans->GetSize().x * 0.5f);
	if (trans->GetPosition().y <= h)
	{
		rb->SetVelocity(glm::vec3(rb->GetVelocity().x, -rb->GetVelocity().y, rb->GetVelocity().z));
	}
}
void ProjectilePredictionScene::Render(float a_deltaTime)
{
	//Light
	glm::vec3 lightPos = glm::vec3(100000, 50000, 0);
	glm::vec3 lightColour = glm::vec3(1.0f, 0.75f, 0.75f);

	//Terrain
	m_TerrainShader.Use();
	m_TerrainShader.SendMat4("view", 1, m_CurrentCamera->GetView());
	m_TerrainShader.SendVec3("cameraPos", 1, m_CurrentCamera->GetPosition());
	m_TerrainShader.SendVec3("lightPos", 1, lightPos);
	m_TerrainShader.SendVec3("lightColour", 1, lightColour);
	m_TerrainShader.SendFloat("specularStr", 0.25f);
	m_TerrainShader.SendFloat("ambientStr", 0.1f);
	m_Terrain.DrawLand(m_TerrainShader);

	//Debugger
	m_Debugger.Draw(m_CurrentCamera->GetView());
	m_Debugger.ClearObjects();
}
void ProjectilePredictionScene::UnLoad()
{
	m_TerrainShader.Destroy();
	m_Terrain.Destroy();
}