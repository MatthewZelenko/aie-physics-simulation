#include "Collision Imposters\CollisionImpostersScene.h"
#include "Application.h"
#include <Input.h>
#include "FlyCamera.h"
#include "TrackCamera.h"
#include "PhysicsUtilities.h"
#include "Collision Imposters\Ball.h"
#include "Collision Imposters\Cube.h"

CollisionImpostersScene::CollisionImpostersScene(Application* a_application) : Scene(a_application)
{

}
CollisionImpostersScene::~CollisionImpostersScene()
{

}

void CollisionImpostersScene::Load()
{
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//Camera
	FlyCamera* flyCamera = new FlyCamera(&m_Application->GetWindow(), 200.0f, 0.002f);
	flyCamera->SetInputKeys(GLFW_KEY_W, GLFW_KEY_S, GLFW_KEY_A, GLFW_KEY_D, GLFW_MOUSE_BUTTON_2);
	flyCamera->SetPerspective(45.0f, (float)m_Application->GetWidth() / (float)m_Application->GetHeight(), 0.1f, 10000.0f);
	flyCamera->SetView(glm::vec3(40.0f, 100.0f, 50.0f), glm::vec3(-0.5f, -0.6f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	AddCamera(flyCamera);

	//Debugger
	m_Debugger.Init(m_CurrentCamera->GetProjection());
	m_Debugger.CreateGrid(10, 10, glm::vec4(0.2f, 0.5f, 1.0f, 1.0f));

	//Terrain Shader
	m_TerrainShader.CreateFromFile("resources/shaders/", "Terrain.vert", nullptr, "Terrain.frag");
	m_TerrainShader.Use();
	m_TerrainShader.SendMat4("projection", 1, m_CurrentCamera->GetProjection());
	m_TerrainShader.SendMat4("view", 1, m_CurrentCamera->GetView());

	//Terrain
	m_Terrain.SetLandAmplitude(200.0f);
	m_Terrain.SetLandFrequency(0.005f);
	m_Terrain.SetLandPersistence(0.2f);
	m_Terrain.SetLandOctave(8);
	m_Terrain.SetLandSeed(2481);
	m_Terrain.Init(400, 400, 10.0f, 10.0f);

	for (int i = 0; i < 25; ++i)
	{
		if (i % 2 == 0)
		{
			Ball* ball = new Ball("Ball" + std::to_string(i), glm::vec3(rand() % 10, (2 * i) + 2.0f, rand() % 10), glm::vec3(3.0f));
			Transform* trans = ball->GetComponent<Transform>();
			trans->SetDebugColour(glm::vec4((rand() % 75) * 0.01f + 0.25f, 0.25f, (rand() % 75) * 0.01f + 0.25f, 1.0f));
			RigidBody& rb = ball->CreateComponent<RigidBody>("RigidBody");
			rb.SetMass(8);
			rb.SetDrag(0.001f);
			SphereCollider& sp = ball->CreateComponent<SphereCollider>("collider");
			sp.SetRadius(trans->GetSize().x);
			sp.SetRestitution(0.5f);
			m_CollisionManager.Register(&sp);
			AddGameObject(ball->GetName(), ball);
			rb.ApplyForce(glm::vec3(0, 0, 1000));
		}
		else
		{
			Cube* cube = new Cube("Cube" + std::to_string(i), glm::vec3(rand() % 10, (2 * i) + 2.0f, rand() % 10), glm::vec3(6.0f));
			Transform* trans = cube->GetComponent<Transform>();
			trans->SetDebugType(DebugType::CUBE);
			trans->SetDebugColour(glm::vec4((rand() % 75) * 0.01f + 0.25f, 0.25f, (rand() % 75) * 0.01f + 0.25f, 1.0f));
			RigidBody& rb = cube->CreateComponent<RigidBody>("RigidBody");
			rb.SetMass(8);
			rb.SetDrag(0.001f);
			AABBCollider& aabb = cube->CreateComponent<AABBCollider>("collider");
			aabb.SetSize(trans->GetSize());
			aabb.SetRestitution(0.5f);
			m_CollisionManager.Register(&aabb);
			AddGameObject(cube->GetName(), cube);
			rb.ApplyForce(glm::vec3(0, 0, 1000));
		}
	}
	//SetGravity(glm::vec3(0));
}
void CollisionImpostersScene::ProcessInput(float a_deltaTime)
{
	if (Input::Get()->KeyPressed(GLFW_KEY_E))
	{
		NextCamera();
	}
	if (Input::Get()->KeyPressed(GLFW_KEY_Q))
	{
		PreviousCamera();
	}
	if (Input::Get()->KeyPressed(GLFW_KEY_G))
	{
		if (glm::length(GetGravity()) > 0)
			SetGravity(glm::vec3(0));
		else
			SetGravity(glm::vec3(0, -98, 0));
	}
	if (Input::Get()->KeyDown(GLFW_KEY_EQUAL))
	{
		m_Terrain.AddLandSeed(1);
		m_Terrain.GenerateLand();
	}if (Input::Get()->KeyDown(GLFW_KEY_MINUS))
	{
		m_Terrain.AddLandSeed(-1);
		m_Terrain.GenerateLand();
	}
}
void CollisionImpostersScene::Update(float a_deltaTime)
{
	m_CurrentCamera->Update((float)m_Application->GetDeltaTime());
	for (int i = 0; i < 25; ++i)
	{
		GameObject* obj = GetGameObject("Ball" + std::to_string(i));
		if (obj)
		{
			Transform* trans = obj->GetComponent<Transform>();
			RigidBody* rb = obj->GetComponent<RigidBody>();
			SphereCollider* sb = obj->GetComponent<SphereCollider>();
			glm::vec3 pos = trans->GetPosition();
			float h = m_Terrain.GetLandHeightFromPosition(pos.x, pos.z, true);
			float radius = sb->GetRadius();
			if (pos.y <= h + radius)
			{
				glm::vec3 collisionNormal = m_Terrain.GetLandNormalFromPosition(pos.x, pos.z, true);
				glm::vec3 collisionVector = collisionNormal * (glm::dot(rb->GetVelocity(), collisionNormal));
				glm::vec3 forceVector = collisionVector * 1.0f / (rb->GetInverseMass());
				float res1 = sb->GetRestitution();
				rb->ApplyForce(-forceVector - (forceVector * res1));

				trans->SetPosition(glm::vec3(pos.x, h + radius, pos.z));
			}
		}
		else
		{
			obj = GetGameObject("Cube" + std::to_string(i));
			Transform* trans = obj->GetComponent<Transform>();
			RigidBody* rb = obj->GetComponent<RigidBody>();
			AABBCollider* aabb = obj->GetComponent<AABBCollider>();
			glm::vec3 pos = trans->GetPosition();
			float h = m_Terrain.GetLandHeightFromPosition(pos.x, pos.z, true);
			float halfHeight = aabb->GetSize().y * 0.5f;
			if (pos.y <= h + halfHeight)
			{
				glm::vec3 collisionNormal = m_Terrain.GetLandNormalFromPosition(pos.x, pos.z, true);
				glm::vec3 collisionVector = collisionNormal * (glm::dot(rb->GetVelocity(), collisionNormal));
				glm::vec3 forceVector = collisionVector * 1.0f / (rb->GetInverseMass());
				float res1 = aabb->GetRestitution();
				rb->ApplyForce(-forceVector - (forceVector * res1));

				trans->SetPosition(glm::vec3(pos.x, h + halfHeight, pos.z));
			}
		}
	}
}
void CollisionImpostersScene::Render(float a_deltaTime)
{
	//Light
	glm::vec3 lightPos = glm::vec3(100000, 50000, 0);
	glm::vec3 lightColour = glm::vec3(1.0f, 0.75f, 0.75f);

	//Terrain
	m_TerrainShader.Use();
	m_TerrainShader.SendMat4("view", 1, m_CurrentCamera->GetView());
	m_TerrainShader.SendVec3("cameraPos", 1, m_CurrentCamera->GetPosition());
	m_TerrainShader.SendVec3("lightPos", 1, lightPos);
	m_TerrainShader.SendVec3("lightColour", 1, lightColour);
	m_TerrainShader.SendFloat("specularStr", 0.25f);
	m_TerrainShader.SendFloat("ambientStr", 0.1f);
	m_Terrain.DrawLand(m_TerrainShader);

	//Debugger
	m_Debugger.Draw(m_CurrentCamera->GetView());
	m_Debugger.ClearObjects();
}
void CollisionImpostersScene::UnLoad()
{
	m_TerrainShader.Destroy();
	m_Terrain.Destroy();
}