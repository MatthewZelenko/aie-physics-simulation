#include "Collision Imposters\Ball.h"
#include "Input.h"
#include "GLM\gtx\rotate_vector.hpp"
#include "GameObject.h"

Ball::Ball(std::string a_name, glm::vec3& a_position, glm::vec3& a_size) : GameObject(a_name, a_position, a_size)
{
	
}
Ball::~Ball()
{
}

void Ball::ProcessInput(float a_deltaTime)
{

}
void Ball::Update(float a_deltaTime)
{

}

void Ball::OnCollisionEnter(GameObject* a_collider)
{
	
}
void Ball::OnCollisionStay(GameObject* a_collider)
{
	
}
void Ball::OnCollisionExit(GameObject* a_collider)
{

}