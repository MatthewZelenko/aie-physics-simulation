#include "Collision Imposters\Cube.h"

Cube::Cube(std::string a_name, glm::vec3& a_position, glm::vec3& a_size) : GameObject(a_name, a_position, a_size)
{

}
Cube::~Cube()
{
}

void Cube::ProcessInput(float a_deltaTime)
{

}
void Cube::Update(float a_deltaTime)
{

}

void Cube::OnCollisionEnter(GameObject* a_collider)
{

}
void Cube::OnCollisionStay(GameObject* a_collider)
{

}
void Cube::OnCollisionExit(GameObject* a_collider)
{

}