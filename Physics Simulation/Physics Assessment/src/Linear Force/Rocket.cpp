#include "Linear Force\Rocket.h"
#include "Input.h"
#include "GLM\gtx\rotate_vector.hpp"


Rocket::Rocket(std::string a_name, glm::vec3& a_position, glm::vec3& a_size, float a_fuel) : GameObject(a_name, a_position, a_size)
{
	m_Fuel = a_fuel;
	m_Forward = glm::vec3(0.0f, 1.0f, 0.0f);
}
Rocket::~Rocket()
{
}

void Rocket::ProcessInput(float a_deltaTime)
{
	Transform* trans = GetComponent<Transform>();
	if (Input::Get()->KeyDown(GLFW_KEY_LEFT))
	{
		trans->AddRotation(glm::vec3(0.0f, 0.0f, 5.0f * a_deltaTime));
	}
	if (Input::Get()->KeyDown(GLFW_KEY_RIGHT))
	{
		trans->AddRotation(glm::vec3(0.0f, 0.0f, -5.0f * a_deltaTime));
	}
	if (Input::Get()->KeyDown(GLFW_KEY_UP))
	{
		trans->AddRotation(glm::vec3(-5.0f * a_deltaTime, 0.0f, 0.0f));
	}
	if (Input::Get()->KeyDown(GLFW_KEY_DOWN))
	{
		trans->AddRotation(glm::vec3(5.0f * a_deltaTime, 0.0f, 0.0f));
	}

	m_Forward = trans->GetRotation() * glm::vec3(0.0f, 1.0f, 0.0f);

}
void Rocket::Update(float a_deltaTime)
{

}

float Rocket::TakeFuel(float a_fuel)
{
	m_Fuel -= a_fuel;
	if (m_Fuel < 0)
	{
		return a_fuel + m_Fuel;
	}
	return a_fuel;
}