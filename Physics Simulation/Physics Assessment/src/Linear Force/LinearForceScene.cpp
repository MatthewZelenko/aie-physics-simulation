#include "Linear Force\LinearForceScene.h"
#include "FlyCamera.h"
#include "Application.h"
#include <Input.h>
#include "Linear Force\Rocket.h"
#include "Linear Force\Fuel.h"
#include "TrackCamera.h"

LinearForceScene::LinearForceScene(Application* a_application) : Scene(a_application),
	m_rocketIsActive(false)
{
}
LinearForceScene::~LinearForceScene()
{

}

void LinearForceScene::Load()
{
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//Camera
	TrackCamera* camera = new TrackCamera(&m_Application->GetWindow(), nullptr, glm::vec3());
	camera->SetPerspective(45.0f, (float)m_Application->GetWidth() / (float)m_Application->GetHeight(), 0.1f, 10000.0f);
	AddCamera(camera);

	FlyCamera* flyCamera = new FlyCamera(&m_Application->GetWindow(), 200.0f, 0.002f);
	flyCamera->SetInputKeys(GLFW_KEY_W, GLFW_KEY_S, GLFW_KEY_A, GLFW_KEY_D, GLFW_MOUSE_BUTTON_2);
	flyCamera->SetPerspective(45.0f, (float)m_Application->GetWidth() / (float)m_Application->GetHeight(), 0.1f, 10000.0f);
	flyCamera->SetView(glm::vec3(0.0f, 100.0f, -150.0f), glm::vec3(0.0f, -1.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	AddCamera(flyCamera);

	//Debugger
	m_Debugger.Init(m_CurrentCamera->GetProjection());
	m_Debugger.CreateGrid(10, 10, glm::vec4(0.2f, 0.5f, 1.0f, 1.0f));

	//Terrain Shader
	m_TerrainShader.CreateFromFile("resources/shaders/", "Terrain.vert", nullptr, "Terrain.frag");
	m_TerrainShader.Use();
	m_TerrainShader.SendMat4("projection", 1, m_CurrentCamera->GetProjection());
	m_TerrainShader.SendMat4("view", 1, m_CurrentCamera->GetView());

	//Terrain
	m_Terrain.SetLandAmplitude(200.0f);
	m_Terrain.SetLandFrequency(0.005f);
	m_Terrain.SetLandPersistence(0.2f);
	m_Terrain.SetLandOctave(8);
	m_Terrain.SetLandSeed(150);
	m_Terrain.Init(200, 200, 10.0f, 10.0f);

	Rocket* rocket = new Rocket("Rocket", glm::vec3(0, 4, 0), glm::vec3(10, 14, 10), 20000.0f);
	Transform* trans = rocket->GetComponent<Transform>();
	trans->SetDebugType(DebugType::CUBE);
	trans->SetDebugColour(glm::vec4(0.0f, 0.25f, 0.75f, 1.0f));
	RigidBody& rb = rocket->CreateComponent<RigidBody>("RigidBody");
	rb.SetMass(0.1);
	AddGameObject("Rocket", rocket);
	camera->SetTrackingObject(rocket);
	camera->SetDisplacement(glm::vec3(0.0f, 100.0f, 200.0f));
}
void LinearForceScene::ProcessInput(float a_deltaTime)
{
	m_rocketIsActive = false;
	if (Input::Get()->KeyPressed(GLFW_KEY_E))
	{
		NextCamera();
	}
	if (Input::Get()->KeyPressed(GLFW_KEY_Q))
	{
		PreviousCamera();
	}
	if (Input::Get()->KeyDown(GLFW_KEY_SPACE))
	{
		m_rocketIsActive = true;
	}
}

void LinearForceScene::Update(float a_deltaTime)
{
	m_CurrentCamera->Update((float)m_Application->GetDeltaTime());
	for (std::map<std::string, GameObject*>::iterator iter = m_GameObjects.begin(); iter != m_GameObjects.end(); iter)
	{
		if (iter->second->GetName().find("Fuel") != std::string::npos)
		{
			if (((Fuel*)iter->second)->GetFuel() <= 0)
			{
				delete iter->second;
				iter = m_GameObjects.erase(iter);
				continue;
			}
		}
		++iter;
	}
}
void LinearForceScene::FixedUpdate(float a_timeStep)
{
	if (m_rocketIsActive)
	{
		Rocket* rocket = (Rocket*)GetGameObject("Rocket");
		float fuel = rocket->TakeFuel(0.25f);
		if (fuel > 0)
		{
			static int fuelObjNum = 0;
			Fuel* fuelObj = new Fuel(fuel);
			Transform* trans = fuelObj->GetComponent<Transform>();
			trans->SetPosition(rocket->GetComponent<Transform>()->GetPosition());
			trans->SetDebugColour(glm::vec4(1.0f, 0.25f, 0.0f, 1.0f));
			rocket->GetComponent<RigidBody>()->ApplyForceToOther(fuelObj->GetComponent<RigidBody>(), rocket->GetForward() * fuel);
			AddGameObject(fuelObj->GetName(), fuelObj);
		}
	}
}

void LinearForceScene::Render(float a_deltaTime)
{
	//Light
	glm::vec3 lightPos = glm::vec3(100000, 50000, 0);
	glm::vec3 lightColour = glm::vec3(1.0f, 0.75f, 0.75f);

	//Terrain
	m_TerrainShader.Use();
	m_TerrainShader.SendMat4("view", 1, m_CurrentCamera->GetView());
	m_TerrainShader.SendVec3("cameraPos", 1, m_CurrentCamera->GetPosition());
	m_TerrainShader.SendVec3("lightPos", 1, lightPos);
	m_TerrainShader.SendVec3("lightColour", 1, lightColour);
	m_TerrainShader.SendFloat("specularStr", 0.25f);
	m_TerrainShader.SendFloat("ambientStr", 0.1f);
	m_Terrain.DrawLand(m_TerrainShader);

	//Debugger
	m_Debugger.Draw(m_CurrentCamera->GetView());
	m_Debugger.ClearObjects();
}
void LinearForceScene::UnLoad()
{
	m_TerrainShader.Destroy();
	m_Terrain.Destroy();
}