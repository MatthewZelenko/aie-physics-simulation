#include "Linear Force\Fuel.h"

int Fuel::m_Num = 0;
Fuel::Fuel(float a_fuel) : GameObject("Fuel" + std::to_string(m_Num++))
{
	RigidBody& rb = CreateComponent<RigidBody>("RigidBody");
	rb.SetMass(a_fuel);
	m_MaxFuel = a_fuel;
	m_CurrentFuel = a_fuel;
}
Fuel::~Fuel()
{
}

void Fuel::Update(float a_deltaTime)
{
	if (m_CurrentFuel > 0.0f)
	{
		m_CurrentFuel -= glm::max(0.0f, m_MaxFuel * 0.001f);
		GetComponent<RigidBody>()->SetMass(m_CurrentFuel * 0.01f);
		GetComponent<Transform>()->SetSize(glm::vec3(m_CurrentFuel) * 10.0f);
	}
}