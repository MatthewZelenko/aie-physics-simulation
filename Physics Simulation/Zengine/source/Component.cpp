#include "Component.h"
#include "GameObject.h"


Component::Component(GameObject* a_parent, std::string a_name, std::string a_componentType)
{
	m_ComponentType = a_componentType;
	m_Parent = a_parent;
	m_Name = a_name;
}
Component::~Component()
{
}

GameObject* Component::GetParent() {
	return m_Parent;
}