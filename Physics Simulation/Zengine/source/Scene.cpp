#include "Application.h"
#include "Scene.h"
#include "Transform.h"
#include "RigidBody.h"
#include "Input.h"
#include <iterator>

Scene::Scene(Application* a_application)
{
	m_CurrentCamera = nullptr;
	m_Application = a_application;
	m_Gravity = glm::vec3(0.0f, -98.0f, 0.0f);
	m_SceneTime = 0.0;
}
Scene::~Scene()
{
	ClearScene();
}
void Scene::ClearObjects()
{
	std::map<std::string, GameObject*>::iterator sceneIter = this->m_GameObjects.begin();
	for (; sceneIter != this->m_GameObjects.end(); ++sceneIter)
	{
		(*sceneIter).second->ClearAllComponents();
		delete (*sceneIter).second;
	}
	this->m_GameObjects.clear();
}
void Scene::ClearScene()
{
	m_SceneTime = 0.0;
	ClearObjects();
	m_CollisionManager.Clear();
	m_ResourceManager.ClearShaders();
	m_ResourceManager.ClearTextures();
	m_Debugger.Destroy();
	for (std::vector<Camera*>::iterator iter = m_Cameras.begin(); iter != m_Cameras.end(); ++iter)
	{
		delete *iter;
	}
	m_Cameras.clear();
	m_CurrentCamera = nullptr;
	glfwSetInputMode(&m_Application->GetWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void Scene::FixedUpdate(float a_timeStep)
{
}

void Scene::ProcessObjectInputs(float a_deltaTime)
{
	m_SceneTime += a_deltaTime;
	std::map<std::string, GameObject*>::iterator sceneIter = this->m_GameObjects.begin();
	for (; sceneIter != this->m_GameObjects.end(); ++sceneIter)
	{
		(*sceneIter).second->ProcessInput(a_deltaTime);
	}
}
void Scene::UpdateObjects(float a_deltaTime)
{
	std::map<std::string, GameObject*>::iterator sceneIter = this->m_GameObjects.begin();
	for (; sceneIter != this->m_GameObjects.end(); ++sceneIter)
	{
		(*sceneIter).second->Update(a_deltaTime);
	}
}
void Scene::UpdatePhysicsObjects(float a_timeStep)
{	
	std::map<std::string, GameObject*>::iterator sceneIter;
	for (sceneIter = this->m_GameObjects.begin(); sceneIter != this->m_GameObjects.end(); ++sceneIter)
	{
		std::map<std::string, SpringJoint>* springs = sceneIter->second->GetComponents<SpringJoint>();
		std::map<std::string, SpringJoint>::iterator springsIter = springs->begin();
		for (; springsIter != springs->end(); ++springsIter)
		{
			springsIter->second.Update(a_timeStep);
		}
	}
	for (sceneIter = this->m_GameObjects.begin(); sceneIter != this->m_GameObjects.end(); ++sceneIter)
	{		
		RigidBody* rb = sceneIter->second->GetComponent<RigidBody>();
		if (rb)
		{
			rb->ApplyForce(m_Gravity * rb->GetMass() * a_timeStep);
			rb->Update(a_timeStep);
		}
	}
	m_CollisionManager.Update(a_timeStep);
}
void Scene::RenderObjects(float a_deltaTime)
{
	std::map<std::string, GameObject*>::iterator sceneIter = this->m_GameObjects.begin();
	for (; sceneIter != this->m_GameObjects.end(); ++sceneIter)
	{
		std::map<std::string, Transform>* trans = sceneIter->second->GetComponents<Transform>();
		for (auto &iter : *trans)
		{
			iter.second.DebugRender(m_Debugger);
		}
		//TODO: Render OBJECTS
	}
}

void Scene::AddGameObject(std::string a_name, GameObject* a_obj)
{
	if (m_GameObjects.find(a_name) != m_GameObjects.end())
	{
		delete m_GameObjects[a_name];
	}
	m_GameObjects[a_name] = a_obj;
}
void Scene::RemoveGameObject(std::string a_name)
{
	if(m_GameObjects.find(a_name) != m_GameObjects.end())
		delete m_GameObjects[a_name];
	m_GameObjects.erase(a_name);
}
GameObject* Scene::GetGameObject(std::string a_name)
{
	if (m_GameObjects.find(a_name) != m_GameObjects.end())
		return m_GameObjects[a_name];
	return nullptr;
}
GameObject* Scene::GetGameObject(int index)
{
	if (index >= 0 && index < m_GameObjects.size())
	{
		return std::next(m_GameObjects.begin(), index)->second;
	}
	return nullptr;
}

void Scene::SetName(std::string a_name)
{
	m_Name = a_name;
}
void Scene::SetCamera(Camera* a_camera)
{
	m_CurrentCamera  = a_camera;
}
void Scene::SetCamera(int a_index)
{
	if (a_index >= m_Cameras.size())
	{
		m_CurrentCamera = m_Cameras[m_Cameras.size() - 1];
		m_CurrentCameraIndex = m_Cameras.size() - 1;
	}
	else if (a_index <= 0)
	{
		m_CurrentCamera = m_Cameras[0];
		m_CurrentCameraIndex = 0;
	}
	else
	{
		m_CurrentCamera = m_Cameras[a_index];
		m_CurrentCameraIndex = a_index;
	}
}
void Scene::SetGravity(glm::vec3& a_gravity)
{
	m_Gravity = a_gravity;
}
void Scene::SetSceneTime(double a_time)
{
	m_SceneTime = glm::max(0.0, a_time);
}

void Scene::AddCamera(Camera* a_camera)
{
	m_Cameras.push_back(a_camera);
	if (m_Cameras.size() == 1)
	{
		m_CurrentCamera = a_camera;
		m_CurrentCameraIndex = 0;
	}
}

void Scene::NextCamera()
{
	if (m_Cameras.size() <= 1)
		return;
	m_CurrentCameraIndex++;
	if (m_CurrentCameraIndex > m_Cameras.size() - 1)
	{
		m_CurrentCameraIndex = 0;
	}
	glfwSetInputMode(&m_Application->GetWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	m_CurrentCamera->SetControlsEnabled(false);
	m_CurrentCamera = m_Cameras[m_CurrentCameraIndex];
}
void Scene::PreviousCamera()
{
	if (m_Cameras.size() <= 1)
		return;
	m_CurrentCameraIndex--;
	if (m_CurrentCameraIndex < 0)
	{
		m_CurrentCameraIndex = m_Cameras.size() - 1;
	}
	glfwSetInputMode(&m_Application->GetWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	m_CurrentCamera = m_Cameras[m_CurrentCameraIndex];
}