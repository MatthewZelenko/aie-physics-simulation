#include "SpringJoint.h"
#include <GLM\glm.hpp>
#include "GameObject.h"

SpringJoint::SpringJoint() : Component(nullptr, "", "Spring")
{
	m_EndPoint = nullptr;
	m_Distance = 0.0f;
	m_Damp = 0.2f;
	m_Stiffness = 20.0f;
}
SpringJoint::SpringJoint(GameObject* a_parent, std::string a_name) : Component(a_parent, a_name, "Spring")
{
	m_EndPoint = nullptr;
	m_Distance = 0.0f;
	m_Damp = 0.2f;
	m_Stiffness = 20.0f;
}
SpringJoint::~SpringJoint()
{

}

void SpringJoint::Update(float a_deltaTime)
{
	if (m_EndPoint)
	{
		RigidBody* startPoint = m_Parent->GetComponent<RigidBody>();
		Transform* trans1 = m_Parent->GetComponent<Transform>();
		Transform* trans2 = m_EndPoint->GetParent()->GetComponent<Transform>();

		glm::vec3 deltaPos = trans1->GetPosition() - trans2->GetPosition();
		glm::vec3 deltaVel = startPoint->GetVelocity() - m_EndPoint->GetVelocity();
		float lengthPos = glm::length(deltaPos);
		if (lengthPos < m_Distance)
			return;

		float spring = -m_Stiffness * (lengthPos - m_Distance);
		glm::vec3 damp = -m_Damp * deltaVel;

		glm::vec3 finalForce = (spring  * glm::normalize(deltaPos)) + damp;

		startPoint->ApplyForce(finalForce);
		m_EndPoint->ApplyForce(-finalForce);
	}
}

void SpringJoint::SetEndPoint(RigidBody* a_endPoint)
{
	m_EndPoint = a_endPoint;
	if (m_Distance == 0.0f)
	{
		Transform* trans1 = m_EndPoint->GetParent()->GetComponent<Transform>();
		Transform* trans2 = m_Parent->GetComponent<Transform>();
		if(trans1 && trans2)
			m_Distance = glm::length(trans1->GetPosition() - trans2->GetPosition());
	}
}
void SpringJoint::SetDistance(float a_distance)
{
	m_Distance = a_distance;
}
void SpringJoint::SetDamp(float a_damp)
{
	m_Damp = a_damp;
}
void SpringJoint::SetStiffness(float a_stiffness)
{
	m_Stiffness = a_stiffness;
}