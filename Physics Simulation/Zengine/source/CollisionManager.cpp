#include "CollisionManager.h"
#include "Collider.h"
#include "GameObject.h"
#include "Transform.h"
#include <GLM\gtx\perpendicular.hpp>

CollisionManager::CollisionManager()
{
	m_NextID.reserve(20);
	m_NextID.push_back(0);
}
CollisionManager::~CollisionManager()
{
	Clear();
}
void CollisionManager::Clear()
{
	for (auto &iter : m_Colliders)
	{
		delete iter;
	}
	m_Colliders.clear();
	m_RegisteredColliders.clear();
	m_NextID.clear();
	m_NextID.push_back(0);
}

void CollisionManager::Update(double a_DeltaTime)
{
	for (std::vector<Registered>::iterator iterX = m_RegisteredColliders.begin(); iterX != m_RegisteredColliders.end(); ++iterX)
	{
		Collider* col1 = (*iterX).collider;
		GameObject* parentX = col1->GetParent();
		for (std::vector<Registered>::iterator iterY = iterX + 1; iterY != m_RegisteredColliders.end(); ++iterY)
		{
			Collider* col2 = (*iterY).collider;
			GameObject* parentY = col2->GetParent();

			if ((*iterX).current.size() <= (*iterY).id)
			{
				(*iterX).current.resize((*iterX).current.size() * 2);
				(*iterX).prev.resize((*iterX).prev.size() * 2);
			}
			if ((*iterY).current.size() <= (*iterX).id)
			{
				(*iterY).current.resize((*iterY).current.size() * 2);
				(*iterY).prev.resize((*iterY).prev.size() * 2);
			}
			(*iterX).current[(*iterY).id] = false;
			(*iterY).current[(*iterX).id] = false;
			//Run Collision Check between both colliders
			bool success = CheckCollision(col1, col2);
			//if it is successfull
			if (success)
			{
				(*iterX).current[(*iterY).id] = true;
				(*iterY).current[(*iterX).id] = true;

				//if the collision between these two before did not happen call collision for the first time(Enter Collision)
				if (!(*iterX).prev[(*iterY).id])
					parentX->OnCollisionEnter(parentY);
				//otherwise it is still colliding(Stay Collision)
				else
					parentX->OnCollisionStay(parentY);

				if (!(*iterY).prev[(*iterX).id])
					parentY->OnCollisionEnter(parentX);
				else
					parentY->OnCollisionStay(parentX);
			}
			else
			{
				//if the collision between these two before did happen call collision for the last time(Exit Collision)
				if (parentX && (*iterX).prev[(*iterY).id])
				{
					parentX->OnCollisionExit(parentY);
				}
				if (parentY && (*iterY).prev[(*iterX).id])
				{
					parentY->OnCollisionExit(parentX);
				}
			}
			(*iterX).prev[(*iterY).id] = (*iterX).current[(*iterY).id];
			(*iterY).prev[(*iterX).id] = (*iterY).current[(*iterX).id];
		}
	}
}

SphereCollider& CollisionManager::CreateSphere(GameObject* a_parent, std::string a_name, bool a_register)
{
	SphereCollider* newSphere = new SphereCollider(a_parent, a_name);
	m_Colliders.push_back(newSphere);
	if (a_register)
	{
		Register(newSphere);
	}
	return *newSphere;
}
void CollisionManager::Register(Collider* a_collider)
{
	int& id = m_NextID.back();

	Registered reg{ a_collider, (unsigned int)id++ };

	if (m_NextID.size() > 1)
		m_NextID.erase(m_NextID.end() - 1);
	reg.current.resize(20);
	reg.prev.resize(20);
	m_RegisteredColliders.push_back(reg);
}
void CollisionManager::Unregister(Collider* a_collider)
{
	for (std::vector<Registered>::iterator iter = m_RegisteredColliders.begin(); iter != m_RegisteredColliders.end(); ++iter)
	{
		if ((*iter).collider == a_collider)
		{
			m_NextID.push_back((*iter).id);
			m_RegisteredColliders.erase(iter);
			return;
		}
	}
}

bool CollisionManager::CheckCollision(Collider* col1, Collider* col2)
{
	if (col1->GetColliderType() == "Sphere" && col2->GetColliderType() == "Sphere")
	{
		return SphereIntersectsSphere((SphereCollider*)col1, (SphereCollider*)col2);
	}
	if (col1->GetColliderType() == "AABB" && col2->GetColliderType() == "Sphere")
	{
		return SphereIntersectsAABB((SphereCollider*)col2, (AABBCollider*)col1);
	}
	if (col1->GetColliderType() == "Sphere" && col2->GetColliderType() == "AABB")
	{
		return SphereIntersectsAABB((SphereCollider*)col1, (AABBCollider*)col2);
	}
	if (col1->GetColliderType() == "AABB" && col2->GetColliderType() == "AABB")
	{
		return AABBIntersectsAABB((AABBCollider*)col1, (AABBCollider*)col2);
	}
	return false;
}
void CollisionManager::ResolveCollision(Collider* a_col1, Collider* a_col2, glm::vec3& a_axis, float a_intersection)
{
	GameObject* obj1 = a_col1->GetParent();
	GameObject* obj2 = a_col2->GetParent();
	Transform* t1 = obj1->GetComponent<Transform>();
	Transform* t2 = obj2->GetComponent<Transform>();
	RigidBody* r1 = obj1->GetComponent<RigidBody>();
	RigidBody* r2 = obj2->GetComponent<RigidBody>();

	//Linear
	glm::vec3 collisionNormal = a_axis;
	glm::vec3 relativeVelocity = r2->GetVelocity() - r1->GetVelocity();
	glm::vec3 collisionVector = collisionNormal * (glm::dot(relativeVelocity, collisionNormal));
	glm::vec3 forceVector = collisionVector * 1.0f / (r1->GetInverseMass() + r2->GetInverseMass());

	float res1 = a_col1->GetRestitution();
	float res2 = a_col2->GetRestitution();

	if (r1->IsDynamic())
	{
		float combinedRes1 = 1.0f;
		switch (a_col1->GetCombine())
		{
		case Combine::MINIMUM:
			combinedRes1 = glm::min(res1, res2);
			break;
		case Combine::AVERAGE:
			combinedRes1 = (res1 + res2) * 0.5f;
			break;
		case Combine::MULTIPLY:
			combinedRes1 = res1 * res2;
			break;
		case Combine::MAXIMUM:
			combinedRes1 = glm::max(res1, res2);
			break;
		default:
			break;
		}
		r1->ApplyForce(forceVector + (forceVector * combinedRes1));
	}
	if (r2->IsDynamic())
	{
		float combinedRes2 = 1.0f;
		switch (a_col2->GetCombine())
		{
		case Combine::MINIMUM:
			combinedRes2 = glm::min(res1, res2);
			break;
		case Combine::AVERAGE:
			combinedRes2 = (res1 + res2) * 0.5f;
			break;
		case Combine::MULTIPLY:
			combinedRes2 = res1 * res2;
			break;
		case Combine::MAXIMUM:
			combinedRes2 = glm::max(res1, res2);
			break;
		default:
			break;
		}
		r2->ApplyForce(-forceVector - (forceVector * combinedRes2));
	}

	glm::vec3 seperationVector = collisionNormal * a_intersection * 0.5f;
	if (r1->IsDynamic())
	{
		t1->AddPosition(-seperationVector);
	}
	if (r2->IsDynamic())
	{
		t2->AddPosition(seperationVector);
	}
}
bool CollisionManager::SphereIntersectsSphere(SphereCollider* col1, SphereCollider* col2)
{		
	glm::vec3 delta = col2->GetPosition() - col1->GetPosition();
	float distance = glm::length(delta);
	float intersection = col1->GetRadius() + col2->GetRadius() - distance;
	if (intersection > 0)
	{
		glm::vec3 pointOfContact = glm::normalize(delta) * col1->GetRadius() + col1->GetPosition();
		ResolveCollision(col1, col2, glm::normalize(delta), intersection);
		return true;
	}
	return false;
}
bool CollisionManager::SphereIntersectsAABB(SphereCollider* a_col1, AABBCollider* a_col2)
{
	glm::vec3 delta = a_col1->GetPosition() - a_col2->GetPosition();
	glm::vec3 bHalfSize = a_col2->GetSize() * 0.5f;

	glm::vec3 cVector;
	//x
	cVector.x = glm::dot(delta, glm::vec3(1.0f, 0.0f, 0.0f));
	if (glm::length(cVector.x) > bHalfSize.x)
		cVector.x = bHalfSize.x * (cVector.x / abs(cVector.x));
	//y
	cVector.y = glm::dot(delta, glm::vec3(0.0f, 1.0f, 0.0f));
	if (glm::length(cVector.y) > bHalfSize.y)
		cVector.y = bHalfSize.y * (cVector.y / abs(cVector.y));
	//z
	cVector.z = glm::dot(delta, glm::vec3(0.0f, 0.0f, 1.0f));
	if (glm::length(cVector.z) > bHalfSize.z)
		cVector.z = bHalfSize.z * (cVector.z / abs(cVector.z));

	glm::vec3 cPoint = a_col2->GetPosition() + cVector;
	glm::vec3 intersection = a_col1->GetPosition() - cPoint;
	float intersectionLength = glm::length(intersection);
	if(intersectionLength < a_col1->GetRadius())
	{ 
		ResolveCollision(a_col1, a_col2, -(intersectionLength > 1 ? glm::normalize(intersection) : intersection), a_col1->GetRadius() - intersectionLength);
		return true;
	}
	return false;
}
bool CollisionManager::AABBIntersectsAABB(AABBCollider* a_col1, AABBCollider* a_col2)
{
	glm::vec3 min1 = a_col1->GetMin();
	glm::vec3 min2 = a_col2->GetMin();
	glm::vec3 max1 = a_col1->GetMax();
	glm::vec3 max2 = a_col2->GetMax();

	float minOverlap = 0.0f;
	glm::vec3 minAxis;

	//xOverlap
	float xOverlap = min1.x - max2.x;
	if (xOverlap < 0)
	{
		minOverlap = xOverlap;
		minAxis = glm::vec3(1.0f, 0.0f, 0.0f);
	}
	else
	{
		return false;
	}

	xOverlap = min2.x - max1.x;
	if (xOverlap < 0)
	{
		if (xOverlap > minOverlap)
		{
			minOverlap = xOverlap;
			minAxis = glm::vec3(-1.0f, 0.0f, 0.0f);
		}
	}
	else
	{
		return false;
	}

	//yOverlap
	float yOverlap = min2.y - max1.y;
	if (yOverlap < 0)
	{
		if (yOverlap > minOverlap)
		{
			minOverlap = yOverlap;
			minAxis = glm::vec3(0.0f, -1.0f, 0.0f);
		}
	}
	else
	{
		return false;
	}
	yOverlap = min2.y - max1.y;
	if (yOverlap < 0)
	{
		if (yOverlap > minOverlap)
		{
			minOverlap = yOverlap;
			minAxis = glm::vec3(0.0f, 1.0f, 0.0f);
		}
	}
	else
	{
		return false;
	}

	//zOverlap
	float zOverlap = min2.z - max1.z;
	if (zOverlap < 0)
	{
		if (zOverlap > minOverlap)
		{
			minOverlap = zOverlap;
			minAxis = glm::vec3(0.0f, 0.0f, -1.0f);
		}
	}
	else
	{
		return false;
	}
	zOverlap = min2.z - max1.z;
	if (zOverlap < 0)
	{
		if (zOverlap > minOverlap)
		{
			minOverlap = zOverlap;
			minAxis = glm::vec3(0.0f, 0.0f, 1.0f);
		}
	}
	else
	{
		return false;
	}

	ResolveCollision(a_col1, a_col2, minAxis, minOverlap);
	return true;
}