#include "..\include\Collider.h"
#include "GameObject.h"

Collider::Collider(GameObject* a_parent, std::string a_name, std::string a_type) : Component(a_parent, a_name, "Collider")
{
	m_Type = a_type;
	m_DynamicFriction = 0.6f;
	m_StaticFriction = 0.6f;
	m_Restitution = 1.0f;
	m_Combine = Combine::AVERAGE;
}
Collider::~Collider()
{
}

void Collider::SetPosition(glm::vec3& a_value)
{
	m_Position = a_value;
}

void Collider::SetStaticFriction(float a_friction)
{
	m_StaticFriction = glm::max(0.0f, a_friction);
}
void Collider::SetDynamicFriction(float a_friction)
{
	m_DynamicFriction = glm::max(0.0f, a_friction);
}
void Collider::SetRestitution(float a_restitution)
{
	m_Restitution = glm::max(0.0f, a_restitution);
}
void Collider::SetCombine(Combine a_combine)
{
	m_Combine = a_combine;
}

void Collider::AddPosition(glm::vec3& a_value)
{
	m_Position += a_value;
}
void Collider::AddStaticFriction(float a_friction)
{
	m_StaticFriction = glm::max(0.0f, m_StaticFriction + a_friction);
}
void Collider::AddDynamicFriction(float a_friction)
{
	m_DynamicFriction = glm::max(0.0f, m_DynamicFriction + a_friction);
}
void Collider::AddRestitution(float a_restitution)
{
	m_Restitution = glm::max(0.0f, m_Restitution + a_restitution);
}

glm::vec3 Collider::GetPosition()
{
	if (m_Parent)
		return m_Parent->GetComponent<Transform>()->GetPosition() + m_Position;
	return m_Position;
}