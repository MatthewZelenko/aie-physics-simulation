#include "RigidBody.h"
#include "GameObject.h"
#include "Transform.h"

RigidBody::RigidBody() :Component(nullptr, "", "RigidBody")
{
	m_Mass = 1.0f;
	m_InverseMass = 1.0f;
	m_Drag = 0.0f;
	m_UsableDrag = 1 / (1.0f + m_Drag);
	m_AngularDrag = 0.005f;
	m_UsableAngularDrag = 1 / (1.0f + m_AngularDrag);
	m_MinLinearThreshold = 0.01f;
	m_MinRotationThreshold = 0.00001f;
}
RigidBody::RigidBody(GameObject* a_parent, std::string a_name) : Component(a_parent, a_name, "RigidBody")
{
	m_Mass = 1.0f;
	m_InverseMass = 1.0f;
	m_Drag = 0.0f;
	m_UsableDrag = 1 / (1.0f + m_Drag);
	m_AngularDrag = 0.005f;
	m_UsableAngularDrag = 1 / (1.0f + m_AngularDrag);
	m_MinLinearThreshold = 0.01f;
	m_MinRotationThreshold = 0.00001f;
}
RigidBody::~RigidBody()
{

}

void RigidBody::Update(float a_deltaTime)
{
	Transform* trans = m_Parent->GetComponent<Transform>();

	//Velocity
	if (m_IsDynamic)
	{
		trans->AddPosition(m_Velocity * a_deltaTime);

		m_Velocity *= m_UsableDrag;
		if (glm::length(m_Velocity) < m_MinLinearThreshold)
			m_Velocity = glm::vec3(0.0f);
	}
	else
	{
		m_Velocity = glm::vec3(0.0f);
	}

	//Rotation
	if (m_IsDynamic)
	{
		trans->AddRotation(m_AngularVelocity * a_deltaTime);

		m_AngularVelocity *= m_UsableAngularDrag;
	}
	else
	{
		m_AngularVelocity = glm::vec3(0.0f);
	}
}

void RigidBody::ApplyForce(glm::vec3& a_force)
{
	m_Velocity += a_force / m_Mass;
}
void RigidBody::ApplyForceToOther(RigidBody* a_rigidbody, glm::vec3& a_force)
{
	ApplyForce(a_force);
	a_rigidbody->ApplyForce(-a_force);
}
void RigidBody::ApplyTorque(glm::vec3& a_force)
{
	if(glm::length(a_force) != 0)
		m_AngularVelocity += a_force * m_Parent->GetComponent<Collider>()->GetInverseInertia();
}

void RigidBody::SetMass(float a_mass)
{
	m_Mass = a_mass;
	m_InverseMass = 1.0f / m_Mass;
}
void RigidBody::SetVelocity(glm::vec3& a_velocity)
{
	m_Velocity = a_velocity;
}
void RigidBody::SetAngularVelocity(glm::vec3& a_velocity)
{
	m_AngularVelocity = a_velocity;
}
void RigidBody::SetDrag(float a_drag)
{
	m_Drag = glm::max(0.0f, a_drag);
	m_UsableDrag = 1 / (1.0f + m_Drag);
}
void RigidBody::SetAngularDrag(float a_drag)
{
	m_AngularDrag = glm::max(0.0f, a_drag);
	m_UsableAngularDrag = 1 / (1.0f + m_AngularDrag);
}
void RigidBody::SetMinLinearThreshold(float a_value)
{
	m_MinLinearThreshold = glm::max(0.0f, a_value);
}
void RigidBody::SetMinRotationThreshold(float a_value)
{
	m_MinRotationThreshold = glm::max(0.0f, a_value);
}

void RigidBody::AddMass(float a_mass)
{
	m_Mass += a_mass;
	m_InverseMass = 1.0f / m_Mass;
}
void RigidBody::AddVelocity(glm::vec3& a_velocity)
{
	m_Velocity += a_velocity;
}
void RigidBody::AddAngularVelocity(glm::vec3& a_velocity)
{
	m_AngularVelocity += a_velocity;
}
void RigidBody::AddDrag(float a_drag)
{
	m_Drag = glm::max(0.0f, m_Drag + a_drag); 
	m_UsableDrag = 1 / (1.0f + m_Drag);
}
void RigidBody::AddAngularDrag(float a_drag)
{
	m_AngularDrag = glm::max(0.0f, m_AngularDrag + a_drag);
	m_UsableAngularDrag = 1 / (1.0f + m_AngularDrag);
}
void RigidBody::AddMinLinearThreshold(float a_value)
{
	m_MinLinearThreshold = glm::max(0.0f, m_MinLinearThreshold + a_value);
}
void RigidBody::AddMinRotationThreshold(float a_value)
{
	m_MinRotationThreshold = glm::max(0.0f, m_MinRotationThreshold + a_value);
}