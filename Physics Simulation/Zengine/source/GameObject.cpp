#include "GameObject.h"
#include "Transform.h"
#include "Collider.h"

GameObject::GameObject(std::string a_name)
{
	m_Name = a_name;
	Transform& trans = CreateComponent<Transform>("Transform");
	trans.SetSize(1.0f, 1.0f, 1.0f);
}
GameObject::GameObject(std::string a_name, glm::vec3& a_position, glm::vec3& a_size)
{
	m_Name = a_name;
	Transform& trans = CreateComponent<Transform>("Transform");
	trans.SetPosition(a_position);
	trans.SetSize(a_size);
}
GameObject::~GameObject()
{
}

void GameObject::ClearAllComponents()
{
	m_Transforms.clear();
	m_RigidBodies.clear();
	for (auto iter : m_Colliders)
	{
		delete iter.second;
	}
	m_Colliders.clear();
}

void GameObject::ProcessInput(float a_deltaTime)
{

}
void GameObject::Update(float a_deltaTime)
{

}

void GameObject::OnCollisionEnter(GameObject* a_collider)
{

}
void GameObject::OnCollisionStay(GameObject* a_collider)
{

}
void GameObject::OnCollisionExit(GameObject* a_collider)
{

}