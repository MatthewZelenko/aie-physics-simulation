#include "Transform.h"

Transform::Transform() : Component(nullptr, "", "Transform")
{
	m_Position = glm::vec3();
	m_Size = glm::vec3();
	m_Rotation = glm::quat();
	m_DebugType = DebugType::SPHERE;
	m_DebugColour = glm::vec4(1.0f);
}
Transform::Transform(GameObject* a_parent, std::string a_name) : Component(a_parent, a_name, "Transform")
{
	m_Position = glm::vec3();
	m_Size = glm::vec3();
	m_Rotation = glm::quat();
	m_DebugType = DebugType::SPHERE;
	m_DebugColour = glm::vec4(1.0f);
}
Transform::Transform(GameObject* a_parent, std::string a_name, glm::vec3 a_position, glm::vec3 a_size) : Component(a_parent, a_name, "Transform")
{
	m_Position = a_position;
	m_Size = a_size;
	m_Rotation = glm::quat();
	m_DebugType = DebugType::SPHERE;
	m_DebugColour = glm::vec4(1.0f);
}
Transform::~Transform()
{

}

Transform::Transform(const Transform& other) : Component(m_Parent, m_Name, "Transform")
{
	m_Position = other.m_Position;
	m_Size = other.m_Size;
	m_Rotation = other.m_Rotation;
	m_DebugColour = other.m_DebugColour;
}
Transform& Transform::operator=(const Transform& other)
{
	m_Position = other.m_Position;
	m_Size = other.m_Size;
	m_Rotation = other.m_Rotation;
	m_DebugColour = other.m_DebugColour;
	return *this;
}

void Transform::DebugRender(Debugger& a_debugger)
{
	if (m_DebugType == DebugType::CUBE)
 		a_debugger.AddCube(m_Position, m_Size, m_DebugColour, m_Rotation);
	else
		a_debugger.AddSphere(m_Position, m_Size.x, m_DebugColour, m_Rotation);
}

void Transform::SetDebugType(DebugType a_type)
{
	m_DebugType = a_type;
}
void Transform::SetDebugColour(glm::vec4& a_colour)
{
	m_DebugColour = a_colour;
}

void Transform::SetPosition(float a_x, float a_y, float a_z)
{
	m_Position.x = a_x;
	m_Position.y = a_y;
	m_Position.z = a_z;
}
void Transform::SetPosition(glm::vec3 a_position)
{
	m_Position = a_position;
}
void Transform::SetSize(float a_w, float a_h, float a_l)
{
	m_Size.x = a_w;
	m_Size.y = a_h;
	m_Size.z = a_l;
}
void Transform::SetSize(glm::vec3 a_size)
{
	m_Size = a_size;
}
void Transform::SetRotation(glm::vec3& a_rotation)
{
	m_Rotation = glm::rotate(glm::quat(), glm::length(a_rotation), a_rotation);
}

void Transform::AddPosition(float a_x, float a_y, float a_z)
{
	m_Position.x += a_x;
	m_Position.y += a_y;
	m_Position.z += a_z;
}
void Transform::AddPosition(glm::vec3 a_position)
{
	m_Position += a_position;
}
void Transform::AddSize(float a_w, float a_h, float a_l)
{
	m_Size.x += a_w;
	m_Size.y += a_h;
	m_Size.z += a_l;
}
void Transform::AddSize(glm::vec3 a_size)
{
	m_Size += a_size;
}
void Transform::AddRotation(glm::vec3& a_rotation)
{
	if(glm::length(a_rotation) != 0.0f)
		m_Rotation = glm::rotate(m_Rotation, glm::length(a_rotation), glm::normalize(a_rotation));
}