#include "Terrain.h"
#include <gl_core_4_4.h>
#include <iostream>

Terrain::Terrain() : m_LandActiveBuffer(0), 
m_LandAmplitude(1.0f), m_LandFrequency(1.0f), m_LandPersistence(0.5f), 
m_LandOctaves(4), m_LandSeed(0)
,m_LandIsDirty(true)
{
	m_LandVAO[0] = 0;
	m_LandVAO[1] = 0;
	m_LandVBO[0] = 0;
	m_LandVBO[1] = 0;
}
Terrain::~Terrain()
{
}
void Terrain::Destroy()
{
	m_LandVertices.clear();
	m_UpdateShader.Destroy();
	if (m_LandVAO[0])
		glDeleteVertexArrays(1, &m_LandVAO[0]);
	if (m_LandVAO[1])
		glDeleteVertexArrays(1, &m_LandVAO[1]);
	if (m_LandVBO[0])
		glDeleteBuffers(1, &m_LandVBO[0]);
	if (m_LandVBO[1])
		glDeleteBuffers(1, &m_LandVBO[1]);
}

void Terrain::Init(unsigned int a_rows, unsigned int a_cols, float a_width, float a_height)
{	
	Destroy();
	const char* varyings[] = { "gPosition", "gNormal", "gTexCoord" };
	m_UpdateShader.CreateFromFile("../Zengine/resources/shaders/", "TerrainUpdate.vert", "TerrainUpdate.geom", nullptr, varyings, 3);
	m_Rows = a_rows;
	m_Width = a_width;
	m_Height = a_height;
	m_Cols = a_cols;
	GenerateGrid();
	CreateBuffers();
	m_LandIsDirty = true;
	GenerateLand();
}
void Terrain::GenerateLand()
{
	if (m_LandIsDirty)
	{
		m_UpdateShader.Use();
		m_UpdateShader.SendFloat("seed", m_LandSeed);
		m_UpdateShader.SendInt("octaves", m_LandOctaves);
		m_UpdateShader.SendFloat("amplitude", m_LandAmplitude);
		m_UpdateShader.SendFloat("frequency", m_LandFrequency);
		m_UpdateShader.SendFloat("persistence", m_LandPersistence);

		glEnable(GL_RASTERIZER_DISCARD);
		glBindVertexArray(m_LandVAO[m_LandActiveBuffer]);
		unsigned int otherBuffer = (m_LandActiveBuffer + 1) % 2;

		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_LandVBO[otherBuffer]);
		glBeginTransformFeedback(GL_TRIANGLES);
		glDrawArrays(GL_TRIANGLES, 0, m_LandVertices.size());
		glEndTransformFeedback();
		glDisable(GL_RASTERIZER_DISCARD);
		glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_LandVertices.size() * sizeof(Vertex), m_LandVertices.data());
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);
		glBindVertexArray(0);
		m_LandActiveBuffer = otherBuffer;
		m_LandIsDirty = false;
	}
}

void Terrain::DrawLand(Shader a_shader)
{
	if (m_LandVAO[m_LandActiveBuffer])
	{
		glBindVertexArray(m_LandVAO[m_LandActiveBuffer]);
		glDrawArrays(GL_TRIANGLES, 0, m_LandVertices.size());
	}
}

void Terrain::SetLandAmplitude(float a_amplitude)
{
	if (m_LandAmplitude != a_amplitude)
	{
		m_LandAmplitude = a_amplitude;
		m_LandIsDirty = true;
	}
}
void Terrain::SetLandPersistence(float a_persistence)
{
	if (m_LandPersistence != a_persistence)
	{
		m_LandPersistence = a_persistence;
		m_LandIsDirty = true;
	}
}
void Terrain::SetLandFrequency(float a_frequency)
{
	if (m_LandFrequency != a_frequency)
	{
		m_LandFrequency = a_frequency;
		m_LandIsDirty = true;
	}
}
void Terrain::SetLandOctave(unsigned int a_octave)
{
	if (m_LandOctaves != a_octave)
	{
		m_LandOctaves = a_octave;
		m_LandIsDirty = true;
	}
}
void Terrain::SetLandSeed(float a_seed)
{
	if (m_LandSeed != a_seed)
	{
		m_LandSeed = a_seed;
		m_LandIsDirty = true;
	}
}

void Terrain::AddLandAmplitude(float a_amplitude)
{
	if (a_amplitude != 0.0f)
	{
		m_LandAmplitude += a_amplitude;
		m_LandIsDirty = true;
	}
}
void Terrain::AddLandPersistence(float a_persistence)
{
	if (a_persistence != 0.0f)
	{
		m_LandPersistence += a_persistence;
		m_LandIsDirty = true;
	}
}
void Terrain::AddLandFrequency(float a_frequency)
{
	if (a_frequency != 0.0f)
	{
		m_LandFrequency += a_frequency;
		m_LandIsDirty = true;
	}
}
void Terrain::AddLandOctave(unsigned int a_octave)
{
	if (a_octave != 0.0f)
	{
		m_LandOctaves += a_octave;
		m_LandIsDirty = true;
	}
}
void Terrain::AddLandSeed(float a_seed)
{
	if (a_seed != 0.0f)
	{
		m_LandSeed += a_seed;
		m_LandIsDirty = true;
	}
}

float Terrain::GetLandHeightFromPosition(float a_x, float a_z, bool precision)
{
	int xIndex = (int)glm::round((a_x / m_Width) + (m_Rows / 2.0f));
	int zIndex = (int)glm::round((a_z / m_Height) + (m_Cols / 2.0f));
	//top left
	int index1 = (zIndex * m_Rows + xIndex) * 6;
	//top right
	int index2 = index1 + 1;
	//bototm right
	int index3 = index1 + 2;
	//bottom left
	int index4 = index1 + 4;

	if (xIndex >= 0 && xIndex < (int)m_Rows && zIndex >= 0 && zIndex < (int)m_Cols)
	{
		if (precision)
		{
			//top left
			glm::vec3 pos0 = m_LandVertices[index1].Position;
			//top right
			glm::vec3 pos1 = m_LandVertices[index2].Position;
			//bottom right
			glm::vec3 pos2 = m_LandVertices[index3].Position;
			//bottom left
			glm::vec3 pos3 = m_LandVertices[index4].Position;

			float xRange = m_Width;
			float yRange = m_Height;

			//project a_x - pos.x / xRange
			float xDot = glm::dot(-1.0f, (a_x - pos1.x) / xRange);
			float yDot = glm::dot(1.0f, (a_z - pos1.z) / yRange);

			if (xDot + yDot <= 1.0f)
			{
				//first triangle pos0 pos1, pos2
				glm::vec3 v1 = pos1 - pos0;
				glm::vec3 v2 = pos2 - pos1;
				glm::vec3 cross = glm::normalize(glm::cross(v1, v2));
				return pos1.y - ((a_x - pos1.x) * cross.x + (a_z - pos1.z) * cross.z) / cross.y;
			}
			else
			{
				//Second Triangle pos2, pos3, pos0
				glm::vec3 v1 = pos2 - pos3;
				glm::vec3 v2 = pos3 - pos0;
				glm::vec3 cross = glm::normalize(glm::cross(v1, v2));
				return pos3.y - ((a_x - pos3.x) * cross.x + (a_z - pos3.z) * cross.z) / cross.y;
			}
		}
		else
		{
			float y = (m_LandVertices[index1].Position.y + m_LandVertices[index3].Position.y) / 2.0f;
			return y;
		}
	}
	return 0;
}
glm::vec3 Terrain::GetLandNormalFromPosition(int a_x, int a_z, bool precision)
{
	int xIndex = (int)glm::round((a_x / m_Width) + (m_Rows / 2.0f));
	int zIndex = (int)glm::round((a_z / m_Height) + (m_Cols / 2.0f));
	//top left
	int index1 = (zIndex * m_Rows + xIndex) * 6;
	//top right
	int index2 = index1 + 1;
	//bototm right
	int index3 = index1 + 2;

	if (xIndex >= 0 && xIndex < (int)m_Rows && zIndex >= 0 && zIndex < (int)m_Cols)
	{
		if (precision)
		{
			//top left
			glm::vec3 norm0 = m_LandVertices[index1].Normal;
			//top right
			glm::vec3 pos1 = m_LandVertices[index2].Position;
			//bottom right
			glm::vec3 norm2 = m_LandVertices[index3].Normal;

			float xRange = m_Width;
			float yRange = m_Height;

			//project a_x - pos.x / xRange
			float xDot = glm::dot(-1.0f, (a_x - pos1.x) / xRange);
			float yDot = glm::dot(1.0f, (a_z - pos1.z) / yRange);

			if (xDot + yDot <= 1.0f)
			{
				return norm0;
			}
			else
			{
				return norm2;
			}
		}
		else
		{
			
			return (m_LandVertices[index1].Normal + m_LandVertices[index3].Normal) / 2.0f;
		}
	}
	return glm::vec3(0.0f, 1.0f, 0.0f);
}
glm::vec3 Terrain::GetLandPositionFromPosition(int a_x, int a_z)
{
	int xIndex = (int)(((float)a_x / m_Width) + ((float)m_Rows / 2.0f));
	int zIndex = (int)(((float)a_z / m_Height) + ((float)m_Cols / 2.0f));

	int index1 = (zIndex * m_Rows + xIndex) * 6;
	int index2 = index1 + 2;
	if (xIndex >= 0 && xIndex < (int)m_Rows && zIndex >= 0 && zIndex < (int)m_Cols)
	{
		glm::vec3 n = (m_LandVertices[index1].Position + m_LandVertices[index2].Position) / 2.0f;
		return n;
	}
	else return glm::vec3(0);
}

float Terrain::GetLandHeightFromIndex(int a_x, int a_z)
{
	int index1 = (a_z * m_Rows + a_x) * 6;
	int index2 = index1 + 2;
	if (a_x >= 0 && a_x < (int)m_Rows && a_z >= 0 && a_z < (int)m_Cols)
	{
		float y = (m_LandVertices[index1].Position.y + m_LandVertices[index2].Position.y) / 2.0f;
		return y;
	}
	else return 0;
}
glm::vec3 Terrain::GetLandNormalFromIndex(int a_x, int a_z)
{
	int index1 = (a_z * m_Rows + a_x) * 6;
	if (a_x >= 0 && a_x < (int)m_Rows && a_z >= 0 && a_z < (int)m_Cols)
	{
		glm::vec3 n = m_LandVertices[index1].Normal;
		return n;
	}
	else return glm::vec3(0);
}
glm::vec3 Terrain::GetLandPositionFromIndex(int a_x, int a_z)
{
	int index1 = (a_z * m_Rows + a_x) * 6;
	int index2 = index1 + 2;
	if (a_x >= 0 && a_x < (int)m_Rows && a_z >= 0 && a_z < (int)m_Cols)
	{
		glm::vec3 n = (m_LandVertices[index1].Position + m_LandVertices[index2].Position) / 2.0f;
		return n;
	}
	else return glm::vec3(0);
}

void Terrain::GenerateGrid()
{
	float texCoordX = 1.0f / (m_Rows - 1.0f);
	float texCoordY = 1.0f / (m_Cols - 1.0f);
	float halfWidth = m_Width / 2.0f;
	float halfHeight = m_Height / 2.0f;
	int halfRow = m_Rows / 2;
	int halfCol = m_Cols / 2;

	m_LandVertices.reserve(m_Rows * m_Cols);
	for (int z = 0; z < (int)m_Cols; ++z)
	{
		for (int x = 0; x < (int)m_Rows; ++x)
		{
			Vertex vertex;
			//top left
			vertex.Position.x = (float)(x - halfRow) * m_Width - halfWidth;
			vertex.Position.z = (float)(z - halfCol) * m_Height - halfHeight;
			vertex.Position.y = 0;
			vertex.TexCoord.x = (float)x;
			vertex.TexCoord.y = (float)z;
			m_LandVertices.push_back(vertex);
			//top right
			vertex.Position.x = (float)(x - halfRow) * m_Width + halfWidth;
			vertex.Position.z = (float)(z - halfCol) * m_Height - halfHeight;
			vertex.Position.y = 0;
			vertex.TexCoord.x = (float)x + 1.0f;
			vertex.TexCoord.y = (float)z;
			m_LandVertices.push_back(vertex);
			//bottom right
			vertex.Position.x = (float)(x - halfRow) * m_Width + halfWidth;
			vertex.Position.z = (float)(z - halfCol) * m_Height + halfHeight;
			vertex.Position.y = 0;
			vertex.TexCoord.x = (float)x + 1.0f;
			vertex.TexCoord.y = (float)z + 1.0f;
			m_LandVertices.push_back(vertex);
			//bottom right
			vertex.Position.x = (float)(x - halfRow) * m_Width + halfWidth;
			vertex.Position.z = (float)(z - halfCol) * m_Height + halfHeight;
			vertex.Position.y = 0;
			vertex.TexCoord.x = (float)x + 1.0f;
			vertex.TexCoord.y = (float)z + 1.0f;
			m_LandVertices.push_back(vertex);
			//bottom left
			vertex.Position.x = (float)(x - halfRow) * m_Width - halfWidth;
			vertex.Position.z = (float)(z - halfCol) * m_Height + halfHeight;
			vertex.Position.y = 0;
			vertex.TexCoord.x = (float)x;
			vertex.TexCoord.y = (float)z + 1.0f;
			m_LandVertices.push_back(vertex);
			//top left
			vertex.Position.x = (float)(x - halfRow) * m_Width - halfWidth;
			vertex.Position.z = (float)(z - halfCol) * m_Height - halfHeight;
			vertex.Position.y = 0;
			vertex.TexCoord.x = (float)x;
			vertex.TexCoord.y = (float)z;
			m_LandVertices.push_back(vertex);
		}
	}
}
void Terrain::CreateBuffers()
{
	//Terrain
	glGenVertexArrays(2, m_LandVAO);
	glGenBuffers(2, m_LandVBO);
	/////Buffer 1/////
	glBindVertexArray(m_LandVAO[0]);
	glBindBuffer(GL_ARRAY_BUFFER, m_LandVBO[0]);
	glBufferData(GL_ARRAY_BUFFER, m_LandVertices.size() * sizeof(Vertex), &m_LandVertices[0], GL_STREAM_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Position));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoord));
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/////Buffer 2/////
	glBindVertexArray(m_LandVAO[1]);
	glBindBuffer(GL_ARRAY_BUFFER, m_LandVBO[1]);
	glBufferData(GL_ARRAY_BUFFER, m_LandVertices.size() * sizeof(Vertex), &m_LandVertices[0], GL_STREAM_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Position));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoord));
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}