#include "../include/AI/FuzzyMember.h"
#include <GLM\glm.hpp>

namespace Fuzzy
{
#pragma region Member
	Member::Member(MemberType a_type, std::string a_name, float a_maxDOM, float a_startX, float a_endX) : Term('M'), m_Name(a_name), m_Type(a_type), m_StartX(a_startX), m_EndX(a_endX)
	{
		m_MaxDOM = a_maxDOM;
		m_DOM = 0.0f;
	}
	Member::~Member()
	{
	}

	float Member::ORWithDOM(float a_value)
	{
		return m_DOM = glm::max(m_DOM, a_value);
	}
#pragma endregion Member
#pragma region MemberTriangle

	Triangle::Triangle(std::string a_name, float a_x0, float a_x1, float a_x2) : Member(TRIANGLE, a_name, a_x1, a_x0, a_x2), m_X0(a_x1)
	{
	}
	Triangle::~Triangle()
	{
	}

	float Triangle::CalculateDOM(float a_Value)
	{
		if (a_Value <= m_StartX || a_Value >= m_EndX)
			return 0.0f;
		else if (a_Value == m_X0)
			return 1.0f;
		else if (a_Value < m_X0)
		{
			float range = m_X0 - m_StartX;
			return (a_Value - m_StartX) / range;
		}

		float range = m_EndX - m_X0;
		return (a_Value - m_X0) / range;
	}
#pragma endregion MemberTriangle
#pragma region MemberLeftShoulder

	LeftShoulder::LeftShoulder(std::string a_name, float a_x0, float a_x1) : Member(LEFTSHOULDER, a_name, a_x0 * 0.5f, 0.0f, a_x1), m_X0(a_x0)
	{
	}
	LeftShoulder::~LeftShoulder()
	{
	}

	float LeftShoulder::CalculateDOM(float a_Value)
	{
		if (a_Value >= m_EndX)
			return 0.0f;
		else if (a_Value <= m_X0)
			return 1.0f;

		float range = m_EndX - m_X0;
		return (a_Value - m_X0) / range;
	}
#pragma endregion MemberLeftShoulder
#pragma region MemberRightShoulder

	RightShoulder::RightShoulder(std::string a_name, float a_x0, float a_x1, float a_x2) : Member(RIGHTSHOUDLER, a_name, (a_x1 + a_x2) * 0.5f, a_x0, a_x2), m_X0(a_x1)
	{
	}
	RightShoulder::~RightShoulder()
	{
	}

	float RightShoulder::CalculateDOM(float a_Value)
	{
		if (a_Value <= m_StartX)
			return 0.0f;
		else if (a_Value >= m_X0)
			return 1.0f;

		float range = m_X0 - m_StartX;
		return (a_Value - m_StartX) / range;
	}
#pragma endregion MemberRightShoulder
}