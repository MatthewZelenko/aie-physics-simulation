#include "../include/AI/FuzzyRule.h"
#include "../include/AI/FuzzyOperator.h"

namespace Fuzzy
{
	Rule::Rule(Term* a_antecedents, Term* a_consequence)
	{
		m_Antecedents = a_antecedents;
		m_Consequence = a_consequence;
	}
	Rule::~Rule()
	{
		
	}

	void Rule::Destroy()
	{
		m_Antecedents->Destroy();
		if (m_Antecedents->GetType() == 'A' || m_Antecedents->GetType() == 'O')
			delete m_Antecedents;

		m_Consequence->Destroy();
		if (m_Consequence->GetType() == 'A' || m_Consequence->GetType() == 'O')
			delete m_Consequence;
	}

	float Rule::CalculateDOM()
	{
		if (m_Consequence && m_Antecedents)
			return m_Consequence->ORWithDOM(m_Antecedents->GetDOM());
		return 0.0f;
	}
}