#include "SphereCollider.h"
#include "GameObject.h"

SphereCollider::SphereCollider(GameObject* a_parent, std::string a_name) : Collider(a_parent, a_name, "Sphere")
{
	m_Radius = 0.0f;
	if (a_parent)
		m_Radius = a_parent->GetComponent<Transform>()->GetSize().x;
	ResetInertia();
}
SphereCollider::~SphereCollider()
{
}

void SphereCollider::Fit(std::vector<glm::vec3>& a_points)
{
	glm::vec3 min;
	glm::vec3 max;
	bool start = true;

	for (auto& p : a_points)
	{
		//min
		if (p.x < min.x || start) min.x = p.x;
		if (p.y < min.y || start) min.y = p.y;
		if (p.z < min.z || start) min.z = p.z;
		//max
		if (p.x > max.x || start) max.x = p.x;
		if (p.y > max.y || start) max.y = p.y;
		if (p.z > max.z || start) max.z = p.z;

		if (start)
			start = false;
	}
	m_Position = (min + max) * 0.5f;
	m_Radius = glm::distance(min, m_Position);
	ResetInertia();
}
void SphereCollider::ResetInertia()
{
	float mass = 1.0f;
	if (m_Parent)
	{
		RigidBody* rb = m_Parent->GetComponent<RigidBody>();
		if (rb)
			mass = rb->GetMass();
	}
	m_Inertia = glm::mat3();
	m_Inertia *= (2.0f / 5.0f) * mass * (m_Radius * m_Radius);
	m_InverseInertia = glm::inverse(m_Inertia);
}

void SphereCollider::SetRadius(float a_value)
{
	m_Radius = a_value;
	ResetInertia();
}
void SphereCollider::ScaleRadius(float a_scale)
{
	m_Radius *= a_scale;
	ResetInertia();
}