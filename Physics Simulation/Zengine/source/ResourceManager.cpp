#include "ResourceManager.h"

ResourceManager::ResourceManager()
{

}
ResourceManager::~ResourceManager()
{
	void ClearTextures();
	//void ClearMeshes();
	void ClearShaders();
}

Texture ResourceManager::LoadTexture(std::string a_name, std::string a_filePath)
{
	Texture texture;
	texture.CreateFromFile(a_filePath.c_str());
	m_Textures[a_name] = texture;
	return texture;
}

Shader ResourceManager::LoadShader(std::string a_name, std::string a_filePath, std::string a_vertex, std::string a_fragment)
{
	Shader shader;
	shader.CreateFromFile(a_filePath.c_str(), a_vertex.c_str(), nullptr, a_fragment.c_str());
	m_Shaders[a_name] = shader;
	return shader;
}

Texture ResourceManager::GetTexture(std::string a_name)
{
	if (m_Textures.find(a_name) != m_Textures.end())
		return m_Textures[a_name];
	else
		return Texture();
}
Shader ResourceManager::GetShader(std::string a_name)
{
	if (m_Shaders.find(a_name) != m_Shaders.end())
		return m_Shaders[a_name];
	else
		return Shader();
}

void ResourceManager::ClearTextures()
{
	for (auto iter : m_Textures)
	{
		iter.second.Destroy();
	}
	m_Textures.clear();
}
void ResourceManager::ClearShaders()
{
	for (auto iter : m_Shaders)
	{
		iter.second.Destroy();
	}
	m_Shaders.clear();
}