#include "..\include\PlaneCollider.h"



PlaneCollider::PlaneCollider(GameObject* a_parent, std::string a_name) : Collider(a_parent, a_name, "Plane")
{
}
PlaneCollider::~PlaneCollider()
{
}

void PlaneCollider::Fit(std::vector<glm::vec3>& a_points)
{

}
void PlaneCollider::ResetInertia()
{

}

void PlaneCollider::SetNormal(glm::vec3& a_normal)
{
	m_Normal = a_normal;
}
void PlaneCollider::SetDistance(float a_distance)
{
	m_Distance = a_distance;
}