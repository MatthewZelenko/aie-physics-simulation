#define STB_IMAGE_IMPLEMENTATION
#include "Texture.h"
#include <STB\stb_image.h>
#include "gl_core_4_4.h"
#include <iostream>
#include <assert.h>
Texture::Texture(): m_ID(0),
m_WrapSParam(GL_REPEAT), m_WrapTParam(GL_REPEAT),
m_MinFilterParam(GL_LINEAR), m_MagFilterParam(GL_LINEAR),
m_ImageFormat(0),
m_ActiveTextureUnit(0)
{

}
Texture::Texture(const Texture& a_other)
{
	m_ID = a_other.m_ID;
	m_Path = a_other.m_Path;
	m_Width = a_other.m_Width;
	m_Height = a_other.m_Height;
	m_WrapSParam = a_other.m_WrapSParam;
	m_WrapTParam = a_other.m_WrapTParam;
	m_MinFilterParam = a_other.m_MinFilterParam;
	m_MagFilterParam = a_other.m_MagFilterParam;
	m_ImageFormat = a_other.m_ImageFormat;
}
Texture::~Texture()
{
}
void Texture::Destroy()
{
	if (m_ID)
	{
		glDeleteTextures(1, &m_ID);
		m_ID = 0;
	}
}

Texture& Texture::operator=(const Texture& a_other)
{
	m_ID = a_other.m_ID;
	m_Path = a_other.m_Path;
	m_Width = a_other.m_Width;
	m_Height = a_other.m_Height;
	m_WrapSParam = a_other.m_WrapSParam;
	m_WrapTParam = a_other.m_WrapTParam;
	m_MinFilterParam = a_other.m_MinFilterParam;
	m_MagFilterParam = a_other.m_MagFilterParam;
	m_ImageFormat = a_other.m_ImageFormat;
	return *this;
}

Texture Texture::Bind(unsigned int a_textureUnit)
{
	assert(m_ID != 0);
	glActiveTexture(GL_TEXTURE0 + a_textureUnit);
	m_ActiveTextureUnit = a_textureUnit;
	glBindTexture(GL_TEXTURE_2D, m_ID);
	return *this;
}
void Texture::UnBind()
{
	glActiveTexture(m_ActiveTextureUnit);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::SetWrapS(int a_param)
{
	if (a_param == GL_REPEAT ||
		a_param == GL_MIRRORED_REPEAT ||
		a_param == GL_CLAMP_TO_EDGE ||
		a_param == GL_CLAMP_TO_BORDER)
	{
		Bind(0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, a_param);
		m_WrapTParam = a_param;
		UnBind();
	}
	else
	{
		std::cout << "Texture Wrap S: Invalid param" << std::endl;
	}
}
void Texture::SetWrapT(int a_param)
{
	if (a_param == GL_REPEAT ||
		a_param == GL_MIRRORED_REPEAT ||
		a_param == GL_CLAMP_TO_EDGE ||
		a_param == GL_CLAMP_TO_BORDER)
	{
		Bind(0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, a_param);
		m_WrapSParam = a_param;
		UnBind();
	}
	else
	{
		std::cout << "Texture Wrap T: Invalid param" << std::endl;
	}
}
void Texture::SetMinFilter(int a_param)
{
	if (a_param == GL_NEAREST ||
		a_param == GL_LINEAR ||
		a_param == GL_NEAREST_MIPMAP_NEAREST ||
		a_param == GL_LINEAR_MIPMAP_NEAREST ||
		a_param == GL_LINEAR_MIPMAP_LINEAR ||
		a_param == GL_NEAREST_MIPMAP_LINEAR)
	{
		Bind(0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, a_param);
		m_MinFilterParam = a_param;
		UnBind();
	}
	else
	{
		std::cout << "Texture Min Filter: Invalid param" << std::endl;
	}
}
void Texture::SetMagFilter(int a_param)
{
	if (a_param == GL_NEAREST ||
		a_param == GL_LINEAR ||
		a_param == GL_NEAREST_MIPMAP_NEAREST ||
		a_param == GL_LINEAR_MIPMAP_NEAREST ||
		a_param == GL_LINEAR_MIPMAP_LINEAR ||
		a_param == GL_NEAREST_MIPMAP_LINEAR)
	{
		Bind(0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, a_param);
		m_MagFilterParam = a_param;
		UnBind();
	}
	else
	{
		std::cout << "Texture Mag Filter: Invalid param" << std::endl;
	}
}

Texture Texture::CreateFromFile(const std::string& a_filePath)
{
	Destroy();
	m_Path = a_filePath;
	glGenTextures(1, &m_ID);
	Bind(0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_WrapSParam);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_WrapTParam);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_MinFilterParam);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_MagFilterParam);

	int width = 0;
	int height = 0;
	m_Width = width;
	m_Height = height;
	unsigned char* image = stbi_load(a_filePath.c_str(), &width, &height, &m_ImageFormat, STBI_default);
	unsigned int alpha = GL_RGB;
	if (m_ImageFormat == 4)
		alpha = GL_RGBA;

	glTexImage2D(GL_TEXTURE_2D, 0, alpha, width, height, 0, alpha, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(image);
	UnBind();
	return *this;
}