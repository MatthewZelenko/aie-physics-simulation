#include "AABBCollider.h"
#include "GameObject.h"

AABBCollider::AABBCollider(GameObject* a_parent, std::string a_name) : Collider(a_parent, a_name, "AABB")
{
	ResetInertia();
}
AABBCollider::~AABBCollider()
{
}

void AABBCollider::Fit(std::vector<glm::vec3>& a_points)
{
	glm::vec3 min;
	glm::vec3 max;
	bool start = true;

	for (auto& p: a_points)
	{
		//min
		if (p.x < min.x || start) min.x = p.x;
		if (p.y < min.y || start) min.y = p.y;
		if (p.z < min.z || start) min.z = p.z;
		//max
		if (p.x > max.x || start) max.x = p.x;
		if (p.y > max.y || start) max.y = p.y;
		if (p.z > max.z || start) max.z = p.z;

		if (start)
			start = false;
	}
	m_Size = glm::abs(max - min);
	m_Position = (min + max) * 0.5f;
	ResetInertia();
}


void AABBCollider::ResetInertia()
{
	float mass = 1.0f;
	if (m_Parent)
	{
		RigidBody* rb = m_Parent->GetComponent<RigidBody>();
		if (rb)
			mass = rb->GetMass();
	}
	m_Inertia = glm::mat3();
	m_Inertia[0][0] = 0.83 * mass * ((m_Size.y * m_Size.y) + (m_Size.z * m_Size.z));
	m_Inertia[1][1] = 0.83 * mass * ((m_Size.x * m_Size.x) + (m_Size.z * m_Size.z));
	m_Inertia[2][2] = 0.83 * mass * ((m_Size.x * m_Size.x) + (m_Size.y * m_Size.y));
	m_InverseInertia = glm::inverse(m_Inertia);
}

void AABBCollider::SetSize(glm::vec3& a_value)
{
	m_Size = a_value;
	ResetInertia();
}
glm::vec3 AABBCollider::GetMin()
{
	return GetPosition() - (m_Size * 0.5f);
}
glm::vec3 AABBCollider::GetMax()
{
	return GetPosition() + (m_Size * 0.5f);
}