#include "Shader.h"
#include "gl_core_4_4.h"
#include <GLM\gtc\type_ptr.hpp>
#include <fstream>
#include <sstream>
#include <iostream>

Shader::Shader() :
	m_ID(0),
	m_Dir("")
{
}
Shader::~Shader()
{
}
void Shader::Destroy()
{
	if (m_ID)
		glDeleteProgram(m_ID);
	m_ID = 0;
	m_Dir = "";
}

Shader Shader::CreateFromString(const char* a_vertex, const char* a_geometry, const char* a_fragment, const char* a_varyings[], const int a_varyingsSize)
{
	Destroy();
	int success = 0;
	char infoLog[512];
	unsigned int vertexID = 0;
	unsigned int fragmentID = 0;
	unsigned int geometryID = 0;

	//Vertex Shader
	if (a_vertex)
	{
		vertexID = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexID, 1, &a_vertex, 0);
		glCompileShader(vertexID);

		glGetShaderiv(vertexID, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(vertexID, 512, 0, infoLog);
			std::cout << "Vertex: Could not create shader (" << infoLog << ")" << std::endl;
		}
	}
	//Fragment Shader
	if (a_fragment)
	{
		fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentID, 1, &a_fragment, 0);
		glCompileShader(fragmentID);
		glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(fragmentID, 512, 0, infoLog);
			std::cout << "Fragment: Could not create shader (" << infoLog << ")" << std::endl;
		}
	}
	//geometry
	if (a_geometry)
	{
		geometryID = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(geometryID, 1, &a_geometry, 0);
		glCompileShader(geometryID);
		glGetShaderiv(geometryID, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(geometryID, 512, 0, infoLog);
			std::cout << "Geometry: Could not create shader (" << infoLog << ")" << std::endl;
		}
	}

	//program
	m_ID = glCreateProgram();
	if (vertexID)
		glAttachShader(m_ID, vertexID);
	if (fragmentID)
		glAttachShader(m_ID, fragmentID);
	if (geometryID)
		glAttachShader(m_ID, geometryID);

	if(a_varyingsSize > 0 && a_varyings != nullptr)
		glTransformFeedbackVaryings(m_ID, a_varyingsSize, a_varyings, GL_INTERLEAVED_ATTRIBS);

	glLinkProgram(m_ID);
	glGetProgramiv(m_ID, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(m_ID, 512, 0, infoLog);
		std::cout << "Program: Could not link shaders (" << infoLog << ")" << std::endl;
	}

	//dump
	if (vertexID)
		glDeleteShader(vertexID);
	if (fragmentID)
		glDeleteShader(fragmentID);
	if (geometryID)
		glDeleteShader(geometryID);
	assert(success && "Shader creation did not succeed");
	return *this;
}
Shader Shader::CreateFromFile(const char* a_dir, const char* a_vertex, const char* a_geometry, const char* a_fragment, const char* a_varyings[], const int a_varyingsSize)
{
	bool success = true;
	std::string vertexString, fragmentString, geometryString;
	m_Dir = a_dir;

	if (!a_dir)
	{
		std::cout << "Shader::Directory: Could not read directory: " << a_dir << std::endl;
		success = false;
	}
	if (a_vertex)
	{
		std::ifstream vertexFile;
		std::stringstream vertexStream;

		std::string path(a_dir);
		path += a_vertex;
		vertexFile.open(path);
		if (!vertexFile.good())
		{
			std::cout << "Shader::Vertex: Could not read file: " << path << std::endl;
			success = false;
		}

		vertexStream << vertexFile.rdbuf();
		vertexString = vertexStream.str();

		vertexFile.close();
	}
	if (a_fragment)
	{
		std::ifstream fragmentFile;
		std::stringstream fragmentStream;

		std::string path(a_dir);
		path += a_fragment;
		fragmentFile.open(path);
		if (!fragmentFile.good())
		{
			std::cout << "Shader::Fragment: Could not read file: " << path << std::endl;
			success = false;
		}

		fragmentStream << fragmentFile.rdbuf();
		fragmentString = fragmentStream.str();

		fragmentFile.close();
	}
	if (a_geometry)
	{
		std::ifstream geometryFile;
		std::stringstream geometryStream;

		std::string path(a_dir);
		path += a_geometry;
		geometryFile.open(path);
		if (!geometryFile.good())
		{
			std::cout << "Shader::Geometry: Could not read file: " << path << std::endl;
			success = false;
		}

		geometryStream << geometryFile.rdbuf();
		geometryString = geometryStream.str();

		geometryFile.close();
	}
	assert(success);
	const char* vertexCode = nullptr;
	const char* fragmentCode = nullptr;
	const char* geometryCode = nullptr;
	if (vertexString != "")
		vertexCode = vertexString.c_str();
	if (fragmentString != "")
		fragmentCode = fragmentString.c_str();
	if (geometryString != "")
		geometryCode = geometryString.c_str();
	CreateFromString(vertexCode, geometryCode, fragmentCode, a_varyings, a_varyingsSize);

	return *this;
}

void Shader::Use()
{
	assert(m_ID);
	glUseProgram(m_ID);
}

void Shader::SendInt(char* a_location, int a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	if (loc < 0)
		std::cout << "Shader: (" << m_Dir << ") Could not find location" << std::endl;
	glUniform1i(loc, a_value);
}
void Shader::SendUint(char* a_location, unsigned int a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	if (loc < 0)
		std::cout << "Shader: (" << m_Dir << ") Could not find location" << std::endl;
	glUniform1ui(loc, a_value);
}
void Shader::SendFloat(char* a_location, float a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	if (loc < 0)
		std::cout << "Shader: (" << m_Dir << ") Could not find location" << std::endl;
	glUniform1f(loc, a_value);
}
void Shader::SendBool(char* a_location, bool a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	if (loc < 0)
		std::cout << "Shader: (" << m_Dir << ") Could not find location" << std::endl;
	glUniform1i(loc, a_value);
}
void Shader::SendMat4(char* a_location, int a_count, const glm::mat4& a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	if (loc < 0)
		std::cout << "Shader: (" << m_Dir << ") Could not find location" << std::endl;
	glUniformMatrix4fv(loc, a_count, GL_FALSE, glm::value_ptr(a_value));
}
void Shader::SendMat3(char* a_location, int a_count, const glm::mat3& a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	if (loc < 0)
		std::cout << "Shader: (" << m_Dir << ") Could not find location" << std::endl;
	glUniformMatrix3fv(loc, a_count, GL_FALSE, glm::value_ptr(a_value));
}
void Shader::SendMat2(char* a_location, int a_count, const glm::mat2& a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	if (loc < 0)
		std::cout << "Shader: (" << m_Dir << ") Could not find location" << std::endl;
	glUniformMatrix2fv(loc, a_count, GL_FALSE, glm::value_ptr(a_value));
}
void Shader::SendVec4(char* a_location, int a_count, const glm::vec4& a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	if (loc < 0)
		std::cout << "Shader: (" << m_Dir << ") Could not find location" << std::endl;
	glUniform4fv(loc, a_count, glm::value_ptr(a_value));
}
void Shader::SendVec3(char* a_location, int a_count, const glm::vec3& a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	if (loc < 0)
		std::cout << "Shader: (" << m_Dir << ") Could not find location" << std::endl;
	glUniform3fv(loc, a_count, glm::value_ptr(a_value));
}
void Shader::SendVec2(char* a_location, int a_count, const glm::vec2& a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	if (loc < 0)
		std::cout << "Shader: (" << m_Dir << ") Could not find location" << std::endl;
	glUniform2fv(loc, a_count, glm::value_ptr(a_value));
}