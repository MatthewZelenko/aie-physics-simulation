#include "FPSCamera.h"
#include "Input.h"
#include "GLFW\glfw3.h"

FPSCamera::FPSCamera() : Camera(nullptr, "FPS")
{
	m_ForwardKey = -1;
	m_BackwardKey = -1;
	m_LeftKey = -1;
	m_RightKey = -1;
	m_JumpKey = -1;
	m_Mass = -10.0f;
	m_IsGrounded = false;
	m_Acceleration = 0.0f;
	m_JumpPower = 100.0f;
	m_MouseSensitivity = 0.0f;
	m_Window = nullptr;
	m_ControlEnabled = false;
	m_ControlEnableKey = -1;
}
FPSCamera::FPSCamera(GLFWwindow* a_window, float a_acceleration, float a_maxSpeed, float a_mouseSensitivity, float a_mass) : Camera(a_window, "FPS")
{
	m_ForwardKey = -1;
	m_BackwardKey = -1;
	m_LeftKey = -1;
	m_RightKey = -1;
	m_JumpKey = -1;
	m_Acceleration = a_acceleration;
	m_Mass = a_mass;
	m_MaxSpeed = a_maxSpeed;
	m_JumpPower = 100.0f;
	m_MouseSensitivity = a_mouseSensitivity;
	m_Window = a_window;
	m_ControlEnabled = false;
	m_ControlEnableKey = -1;
}
FPSCamera::~FPSCamera()
{
	glfwSetInputMode(m_Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void FPSCamera::Update(float a_deltaTime)
{
	//Grounding
	if (m_IsGrounded)
	{
		m_TempVelocity.y = 0;
	}
	else
	{
		float maxFall = -m_Mass * 20.0f;
		m_TempVelocity.y = glm::max(m_TempVelocity.y - m_Mass * 0.1f, maxFall);
	}

	//Setting camera mode
	if (Input::Get()->MousePressed(m_ControlEnableKey))
	{
		m_ControlEnabled = !m_ControlEnabled;
		if (m_ControlEnabled)
		{
			glfwSetInputMode(m_Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		}
		else
		{
			glfwSetInputMode(m_Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
	}
	if (m_ControlEnabled)
	{
		//Mouse Controlling
		double xOffset = Input::Get()->GetMouseOffsetX();
		double yOffset = Input::Get()->GetMouseOffsetY();

		if (xOffset)
		{
			RotateAroundWorldUp((float)xOffset * m_MouseSensitivity);
		}
		if (yOffset)
		{
			float dot = glm::dot(m_Forward, glm::vec3(0.0f, -1.0f, 0.0f));
			if ((dot < -0.999f && yOffset > 0.0f) || (dot > 0.999f && yOffset < 0.0f))
			{
				yOffset = 0.0f;
			}
			RotateAroundRight((float)-yOffset * m_MouseSensitivity);
		}

		//Keyboard Controlling
		//If on the ground
		if (m_IsGrounded)
		{
			float speed = m_Acceleration * 1 / m_Mass;
			//Moving
			if (Input::Get()->KeyDown(m_ForwardKey))
			{
				m_TempVelocity.z = glm::min(m_TempVelocity.z + speed, m_MaxSpeed);
			}
			if (Input::Get()->KeyDown(m_BackwardKey))
			{
				m_TempVelocity.z = glm::max(m_TempVelocity.z - speed, -m_MaxSpeed);
			}
			if (Input::Get()->KeyDown(m_LeftKey))
			{
				m_TempVelocity.x = glm::max(m_TempVelocity.x - speed, -m_MaxSpeed);
			}
			if (Input::Get()->KeyDown(m_RightKey))
			{
				m_TempVelocity.x = glm::min(m_TempVelocity.x + speed, m_MaxSpeed);
			}
		}
	}
	//If on the ground
	if (m_IsGrounded)
	{
		float speed = m_TempVelocity.z / (m_Mass * 0.25f);
		//Release forward and back
		if (m_TempVelocity.z != 0.0f && (Input::Get()->KeyUp(m_ForwardKey) && Input::Get()->KeyUp(m_BackwardKey)) || !m_ControlEnabled)
		{
			if (m_TempVelocity.z > -m_Mass * 0.25f && m_TempVelocity.z < m_Mass * 0.25f)
			{
				m_TempVelocity.z = 0;
			}
			else
			{
				m_TempVelocity.z += -speed;
			}
		}
		speed = m_TempVelocity.x / (m_Mass * 0.25f);
		//Release left and right
		if (m_TempVelocity.x != 0.0f && (Input::Get()->KeyUp(m_LeftKey) && Input::Get()->KeyUp(m_RightKey)) || !m_ControlEnabled)
		{
			if (m_TempVelocity.x > -m_Mass * 0.25f && m_TempVelocity.x < m_Mass * 0.25f)
			{
				m_TempVelocity.x = 0;
			}
			else
			{
				m_TempVelocity.x += -speed;
			}
		}
	}

	//for inclination velocity decrease
	m_Velocity = m_TempVelocity;
	if (m_Velocity.x != 0)
		TranslateX(m_Velocity.x * a_deltaTime);
	if (m_Velocity.z != 0)
		TranslateZ(m_Velocity.z * a_deltaTime);
	if (m_Velocity.y != 0)
		TranslateUp(m_Velocity.y * a_deltaTime);

	//Reset up
	SetUp(glm::vec3(0.0f, 1.0f, 0.0f));
}

void FPSCamera::SetInputKeys(int a_forwardKey, int a_backwardKey, int a_leftKey, int a_rightKey, int a_jumpKey, int a_controlEnableKey)
{
	m_ForwardKey = a_forwardKey;
	m_BackwardKey = a_backwardKey;
	m_LeftKey = a_leftKey;
	m_RightKey = a_rightKey;
	m_JumpKey = a_jumpKey;
	SetEnableKey(a_controlEnableKey);
}
void FPSCamera::SetIsGrounded(bool a_value)
{
	m_IsGrounded = a_value;
}
void FPSCamera::SetMaxSpeed(float a_value)
{
	m_MaxSpeed = a_value;
}
void FPSCamera::SetMass(float a_value)
{
	m_Mass = a_value;
}
void FPSCamera::SetJumpPower(float a_value)
{
	m_JumpPower = a_value;
}
void FPSCamera::SetMouseSensitivity(float a_mouseSensitivity)
{
	m_MouseSensitivity = a_mouseSensitivity;
}
void FPSCamera::SetAcceleration(float a_acceleration)
{
	m_Acceleration = a_acceleration;
}