#include "Application.h"
#include "Input.h"
#include <iostream>
#include <thread>

Application::Application(std::string a_gameTitle, unsigned int a_gameWidth, unsigned int a_gameHeight) : m_GameTitle(a_gameTitle), m_GameWidth(a_gameWidth), m_GameHeight(a_gameHeight), m_GameIsRunning(true), m_RunTime(0.0f), m_DeltaTime(0.0f), m_GameWindow(nullptr)
{
	m_TimeStep = 0.01f;
	m_ElapsedTimeStep = 0.0f;
}
Application::~Application()
{
}

void Application::Run()
{
	SystemStartup();
	Load();

	while (m_GameIsRunning && !glfwWindowShouldClose(m_GameWindow))
	{
		SystemUpdate();
		SceneLoad();
		SceneProcessInput();
		ProcessInput();
		SceneUpdate();
		Update();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		SceneRender();
		Render();
		glfwSwapBuffers(m_GameWindow);

		if (Input::Get()->KeyDown(GLFW_KEY_ESCAPE))
		{
			m_GameIsRunning = false;
		}
	}
	SceneUnLoad();
	UnLoad();
	SystemShutDown();
}

void Application::SystemStartup()
{
	bool success = false;
	int error = glfwInit();
	if (error == GL_FALSE)
	{
		std::cout << "Error initializing GLFW." << std::endl;
	}
	else
	{
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		m_GameWindow = glfwCreateWindow(m_GameWidth, m_GameHeight, m_GameTitle.c_str(), nullptr, nullptr);
		if (!m_GameWindow)
		{
			std::cout << "Error creating GLFWwindow." << std::endl;
		}
		else
		{
			glfwMakeContextCurrent(m_GameWindow);
			error = ogl_LoadFunctions();
			if (error == ogl_LoadStatus::ogl_LOAD_FAILED)
			{
				std::cout << "Error initializing gl_core." << std::endl;
			}
			else
			{
				glfwSwapInterval(1);
				glEnable(GL_CULL_FACE);
				glCullFace(GL_FRONT);
				
				glViewport(0, 0, m_GameWidth, m_GameHeight);
				glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
				glEnable(GL_DEPTH_TEST);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				Input::Create();
				Input::Get()->SetMousePosition(m_GameWindow, m_GameWidth / 2.0f, m_GameHeight / 2.0f);
				glfwSetKeyCallback(m_GameWindow, Input::KeyCallBack);
				glfwSetCursorPosCallback(m_GameWindow, Input::CursorCallBack);
				glfwSetMouseButtonCallback(m_GameWindow, Input::MouseCallBack);

				success = true;
			}
		}
	}	
	if (!success)
	{
		system("Pause");
		m_GameIsRunning = false;
	}
}
void Application::SystemUpdate()
{
	double currentTime = glfwGetTime();
	m_DeltaTime = currentTime - m_RunTime;
	m_RunTime = currentTime;
	m_ElapsedTimeStep += m_DeltaTime;

	Input::Get()->UpdateInputs();
	glfwPollEvents();
}
void Application::SystemShutDown()
{
	Input::Destroy();
	glfwTerminate();
}

Scene* Application::AddScene(Scene* scene, std::string name)
{
	scene->SetName(name);
	this->m_Scenes[name] = scene;
	if (this->m_CurrentScenes.empty())
	{
		PushScene(name);
	}
	return scene;
}
void Application::RemoveScene(const std::string name)
{
	Scene* scene = m_Scenes[name];
	if (scene)
	{
		for (std::vector<Scene*>::iterator iter = m_CurrentScenes.begin(); iter != m_CurrentScenes.end(); ++iter)
		{
			if ((*iter)->GetName() == name)
			{
				m_CurrentScenes.erase(iter);
			}
		}
		delete m_Scenes[name];
		m_Scenes.erase(name);
	}
}
void Application::PushScene(std::string name)
{
	Scene* scene = nullptr;
	scene = m_Scenes[name];
	if (scene)
	{
		m_CurrentScenes.push_back(scene);
		m_PushedOnStack = true;
	}
}
void Application::PushScene(Scene* scene)
{
	Scene* pushScene = scene;
	if (pushScene)
	{
		m_CurrentScenes.push_back(pushScene);
		m_PushedOnStack = true;
	}
}

void Application::PopScene()
{
	m_ElapsedTimeStep = 0.0f;
	m_CurrentScenes.back()->UnLoad();
	m_CurrentScenes.back()->ClearScene();
	m_CurrentScenes.pop_back();
}
void Application::PopToScene(std::string name)
{
	Scene* scene = m_CurrentScenes.back();
	while (scene->GetName() != name)
	{
		if (m_CurrentScenes.size() == 1)
		{
			return;
		}
		PopScene();
		scene = m_CurrentScenes.back();
	}
}
void Application::PopAllScenes()
{
	while (!m_CurrentScenes.empty())
	{
		Scene* scene = m_CurrentScenes.back();
		PopScene();
	}
}
void Application::ChangeScene(std::string a_name)
{
	PopScene();
	PushScene(a_name);
}

void Application::SceneLoad()
{
	if (m_PushedOnStack && !m_CurrentScenes.empty())
	{
		m_CurrentScenes.back()->Load();
		m_PushedOnStack = false;
	}
}
void Application::SceneProcessInput()
{
	if (!m_CurrentScenes.empty() && !m_PushedOnStack)
	{
		m_CurrentScenes.back()->ProcessInput(m_DeltaTime);
		m_CurrentScenes.back()->ProcessObjectInputs(m_DeltaTime);
	}
}
void Application::SceneUpdate()
{
	if (!m_CurrentScenes.empty() && !m_PushedOnStack)
	{
		m_CurrentScenes.back()->Update(m_DeltaTime);
		if (m_ElapsedTimeStep > m_TimeStep)
		{
			m_CurrentScenes.back()->FixedUpdate(m_TimeStep);
			m_ElapsedTimeStep -= m_TimeStep;
			m_CurrentScenes.back()->UpdatePhysicsObjects(m_TimeStep);
		}
		m_CurrentScenes.back()->UpdateObjects(m_DeltaTime);
	}
}
void Application::SceneRender()
{
	if (!m_CurrentScenes.empty() && !m_PushedOnStack)
	{
		m_CurrentScenes.back()->RenderObjects(m_DeltaTime);
		m_CurrentScenes.back()->Render(m_DeltaTime);
	}
}
void Application::SceneUnLoad()
{
	this->m_CurrentScenes.clear();
	std::map<std::string, Scene*>::iterator sceneIter = this->m_Scenes.begin();
	for (; sceneIter != this->m_Scenes.end(); ++sceneIter)
	{
		sceneIter->second->UnLoad();
		delete sceneIter->second;
	}
	this->m_Scenes.clear();
}
