#include "TrackCamera.h"
#include "GameObject.h"
#include <GLM\gtc\matrix_transform.hpp>

TrackCamera::TrackCamera() : Camera(nullptr, "Track")
{
	m_object = nullptr;
}
TrackCamera::TrackCamera(GLFWwindow* a_window, GameObject* a_object, glm::vec3& a_displacement) : Camera(m_Window, "Track")
{
	m_object = a_object;
	m_Displacement = a_displacement;
}
TrackCamera::~TrackCamera()
{
}

void TrackCamera::Update(float a_deltaTime)
{
	if (m_object)
	{
		//SetView(glm::vec3(0.0f, 50.0f, 50.0f), glm::vec3(0.0f, -1.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		glm::vec3 pos = m_object->GetComponent<Transform>()->GetPosition();
		glm::vec3 p = pos + m_Displacement;
		SetView(p, pos - p, glm::vec3(0.0f, 1.0f, 0.0f));
	}
}