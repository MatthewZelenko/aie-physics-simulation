#version 330 core

layout(location = 0)in vec4 Position;
layout(location = 1)in vec4 Colour;

uniform vec4 colour = vec4(0.0f, 0.0f, 0.0f, 1.0f);

uniform mat4 projection = mat4(1);
uniform mat4 view = mat4(1);
uniform mat4 model = mat4(1);

out vec4 vColour;

void main()
{
	vColour = Colour * colour;
	gl_Position = projection * view * model * Position;
}