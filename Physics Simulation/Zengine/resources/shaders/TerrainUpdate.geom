#version 330 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 6)out;

in vec3 vPosition[];
in vec3 vNormal[];
in vec2 vTexCoord[];

out vec3 gPosition;
out vec3 gNormal;
out vec2 gTexCoord;

void main()
{
	vec3 v1 = vPosition[2] - vPosition[0];
	vec3 v2 = vPosition[1] - vPosition[0];
	vec3 normal = normalize(cross(v1, v2));

	gPosition = vPosition[0];
	gNormal = normal;
	gTexCoord = vTexCoord[0];
	EmitVertex();
	
	gPosition = vPosition[1];
	gNormal = normal;
	gTexCoord = vTexCoord[1];
	EmitVertex();
	
	gPosition = vPosition[2];
	gNormal = normal;
	gTexCoord = vTexCoord[2];
	EmitVertex();

	EndPrimitive();
}