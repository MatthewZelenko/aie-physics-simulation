#pragma once
#include <vector>
#include <map>
#include <GLM\glm.hpp>
#include "SphereCollider.h"
#include "AABBCollider.h"

class GameObject;
class Collider;

struct Registered {
	Collider* collider;
	unsigned int id;
	std::vector<bool> current;
	std::vector<bool> prev;
};

class CollisionManager
{
public:
	CollisionManager();
	~CollisionManager();
	void Clear();

	void Update(double a_DeltaTime);

	SphereCollider& CreateSphere(GameObject* a_parent, std::string a_name, bool a_register = false);
	AABBCollider& CreateAABB(GameObject* a_parent, std::string a_name, bool a_register = false);
	void Register(Collider* a_collider);
	void Unregister(Collider* a_collider);

private:
	struct MTV
	{
		bool Success = false;
		float Penetration = 0.0f;
		glm::vec3 Axis;
		glm::vec3 ContactPoint;
	};

	bool CheckCollision(Collider* a_col1, Collider* a_col2);
	void ResolveCollision(Collider* col1, Collider* col2, glm::vec3& a_axis, float a_intersection);
	bool SphereIntersectsSphere(SphereCollider* a_col1, SphereCollider* a_col2);
	bool SphereIntersectsAABB(SphereCollider* a_col1, AABBCollider* a_col2);
	bool AABBIntersectsAABB(AABBCollider* a_col1, AABBCollider* a_col2);

	std::vector<Collider*> m_Colliders;
	std::vector<Registered> m_RegisteredColliders;
	std::vector<int> m_NextID;
};