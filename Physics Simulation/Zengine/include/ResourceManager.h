#pragma once
#include <map>
#include <string>
#include <Texture.h>
#include <Shader.h>
class ResourceManager
{
public:
	ResourceManager();
	~ResourceManager();

	Texture LoadTexture(std::string a_name, std::string a_filePath);

	//Mesh LoadMesh(std::string a_name, std::string a_filePath);
	//Mesh LoadShader(Mesh a_mesh);

	Shader LoadShader(std::string a_name, std::string a_filePath, std::string a_vertex, std::string a_fragment);

	Texture GetTexture(std::string a_name);
	Shader GetShader(std::string a_name);

	void ClearTextures();
	//void ClearMeshes();
	void ClearShaders();


private:
	std::map<std::string, Texture> m_Textures;
	//std::map<std::string, Mesh>> m_Meshes;
	std::map<std::string, Shader> m_Shaders;
};