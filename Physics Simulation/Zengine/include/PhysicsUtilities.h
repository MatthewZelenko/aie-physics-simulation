#pragma once
#include <GLM\glm.hpp>

namespace {
	glm::vec3 PredictPosition(glm::vec3& a_startingPosition, glm::vec3& a_startingVelocity, glm::vec3& a_force, float a_time)
	{
		return (0.5f * a_force * (a_time * a_time)) + (a_startingVelocity * a_time) + a_startingPosition - 2.0f;
	}
}