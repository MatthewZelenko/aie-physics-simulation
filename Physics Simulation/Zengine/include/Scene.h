#pragma once
#include <vector>
#include <string>
#include "GameObject.h"
#include "Camera.h"
#include "ResourceManager.h"
#include "CollisionManager.h"
#include "Debugger.h"

class Application;

class Scene
{
public:
	Scene(Application* a_application);
	virtual ~Scene();
	void ClearObjects();
	void ClearScene();

	virtual void Load() = 0;
	virtual void ProcessInput(float a_deltaTime) = 0;
	virtual void Update(float a_deltaTime) = 0;
	virtual void FixedUpdate(float a_timeStep);
	virtual void Render(float a_deltaTime) = 0;
	virtual void UnLoad() = 0;

	void ProcessObjectInputs(float a_deltaTime);
	void UpdateObjects(float a_deltaTime);
	void UpdatePhysicsObjects(float a_timeStep);
	void RenderObjects(float a_deltaTime);

	void AddGameObject(std::string a_name, GameObject* a_obj);
	void RemoveGameObject(std::string a_name);
	GameObject* GetGameObject(std::string a_name);
	GameObject* GetGameObject(int index);

	void PreviousCamera();
	void NextCamera();

	void SetName(std::string a_name);
	void SetCamera(Camera* a_camera);
	void SetCamera(int a_index);
	void SetGravity(glm::vec3& a_gravity);
	void SetSceneTime(double a_time);

	void AddCamera(Camera* a_camera);

	std::string GetName() { return m_Name; }
	Camera* GetCamera(int a_index) { return m_Cameras[a_index]; }
	glm::vec3 GetGravity() { return m_Gravity; }
	double GetSceneTime() { return m_SceneTime; }
	
protected:
	Application* m_Application;
	ResourceManager m_ResourceManager;
	CollisionManager m_CollisionManager;
	Debugger m_Debugger;
	//TODO: vector of cameras
	std::vector<Camera*> m_Cameras;
	Camera* m_CurrentCamera;
	std::map<std::string, GameObject*> m_GameObjects;

private:
	std::string m_Name;
	glm::vec3 m_Gravity;
	int m_CurrentCameraIndex;
	double m_SceneTime;
};