#pragma once
#include "Component.h"
#include <GLM\glm.hpp>
#include "GLM\gtc\quaternion.hpp"
#include "Debugger.h"

enum class DebugType
{
	SPHERE,
	CUBE,
	TOTAL
};

class Transform : public Component
{
public:
	Transform();
	Transform(GameObject* a_parent, std::string a_name);
	Transform(GameObject* a_parent, std::string a_name, glm::vec3 position, glm::vec3 size);
	~Transform();

	Transform(const Transform& other);
	Transform& operator=(const Transform& other);

	void DebugRender(Debugger& a_debugger);

	void SetDebugType(DebugType a_type);
	void SetDebugColour(glm::vec4& a_colour);

	void SetPosition(float a_x, float a_y, float a_z);
	void SetPosition(glm::vec3 a_position);
	void SetSize(float a_w, float a_h, float a_l);
	void SetSize(glm::vec3 a_size);
	void SetRotation(glm::vec3& a_rotation);

	void AddPosition(float a_x, float a_y, float a_z);
	void AddPosition(glm::vec3 a_position);
	void AddSize(float a_w, float a_h, float a_l);
	void AddSize(glm::vec3 a_size);
	void AddRotation(glm::vec3& a_rotation);

	glm::vec3 GetPosition() { return m_Position; }
	glm::vec3 GetSize() { return m_Size; }
	glm::quat GetRotation() { return m_Rotation; }

protected:
	glm::vec3 m_Position;
	glm::vec3 m_Size;
	glm::quat m_Rotation;

	DebugType m_DebugType;
	glm::vec4 m_DebugColour;
};