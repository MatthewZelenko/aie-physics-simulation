#pragma once
#include <string>

class GameObject;

class Component
{
public:
	Component(GameObject* a_parent, std::string a_name, std::string a_componentType);
	virtual ~Component();

	void SetName(std::string a_name) { m_Name = a_name; }
	void SetParent(GameObject* a_parent) { m_Parent = a_parent; }

	std::string GetComponentType() { return m_ComponentType; }
	GameObject* GetParent();
	std::string GetName() { return m_Name; }

protected:
	GameObject* m_Parent;
	std::string m_ComponentType;
	std::string m_Name;
};