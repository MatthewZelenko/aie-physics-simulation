#pragma once
#include "Collider.h"
class PlaneCollider :
	public Collider
{
public:
	PlaneCollider(GameObject* a_parent, std::string a_name);
	~PlaneCollider();

	void Fit(std::vector<glm::vec3>& a_points);
	void ResetInertia();

	void SetNormal(glm::vec3& a_normal);
	void SetDistance(float a_distance);

	glm::vec3 GetNormal() { return m_Normal; }
	float GetDistance() { return m_Distance; }

protected:
	float m_Distance;
	glm::vec3 m_Normal;
};