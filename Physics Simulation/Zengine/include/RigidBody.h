#pragma once
#include "Component.h"
#include "GLM\glm.hpp"

class RigidBody : public Component
{
public:
	RigidBody();
	RigidBody(GameObject* a_parent, std::string a_name);
	~RigidBody();

	void Update(float a_deltaTime);

	void ApplyForce(glm::vec3& a_force);
	void ApplyForceToOther(RigidBody* a_rigidbody, glm::vec3& a_force);
	void ApplyTorque(glm::vec3& a_force);

	void SetDynamic() { m_IsDynamic = true; }
	void SetStatic() { m_IsDynamic = false; }
	void SetMass(float a_mass);
	void SetVelocity(glm::vec3& a_velocity);
	void SetAngularVelocity(glm::vec3& a_velocity);
	void SetDrag(float a_drag);
	void SetAngularDrag(float a_drag);
	void SetMinLinearThreshold(float a_value);
	void SetMinRotationThreshold(float a_value);

	void AddMass(float a_mass);
	void AddVelocity(glm::vec3& a_velocity);
	void AddAngularVelocity(glm::vec3& a_velocity);
	void AddDrag(float a_drag);
	void AddAngularDrag(float a_drag);
	void AddMinLinearThreshold(float a_value);
	void AddMinRotationThreshold(float a_value);

	bool IsDynamic() { return m_IsDynamic; }
	bool IsStatic() { return !m_IsDynamic; }
	glm::vec3 GetVelocity() { return m_Velocity; }
	glm::vec3 GetAngularVelocity() { return m_AngularVelocity; }
	float GetMass() { return m_Mass; }
	float GetInverseMass() { return m_InverseMass; }
	float GetDrag() { return m_Drag; }
	float GetAngularDrag() { return m_AngularDrag; }
	float GetMinLinearThreshold() { return m_MinLinearThreshold; }
	float GetMinRotationThreshold() { return m_MinRotationThreshold; }

private:
	bool m_IsDynamic;
	glm::vec3 m_Velocity;
	glm::vec3 m_AngularVelocity;
	float m_Mass;
	float m_InverseMass;
	float m_Drag;
	float m_UsableDrag;
	float m_AngularDrag;
	float m_UsableAngularDrag;
	float m_MinLinearThreshold;
	float m_MinRotationThreshold;
};