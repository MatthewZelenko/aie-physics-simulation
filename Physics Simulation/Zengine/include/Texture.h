#pragma once
#include <string>

class Texture
{
public:
	Texture();
	Texture(const Texture& a_other);
	~Texture();

	/*Destroys the texture.*/
	void Destroy();

	Texture& operator=(const Texture& a_other);

	/*Creates a texture from file.*/
	Texture CreateFromFile(const std::string& a_filePath);
	/*Activates texture unit then binds it.*/
	Texture Bind(unsigned int a_textureUnit);
	/*UnBinds the texture at the unit it was bound to.*/
	void UnBind();

	/*Sets the wrap mode along the S.*/
	void SetWrapS(int a_param);
	/*Sets the wrap mode along the T.*/
	void SetWrapT(int a_param);
	/*Sets the min filter.*/
	void SetMinFilter(int a_param);
	/*Sets the mag filter.*/
	void SetMagFilter(int a_param);

	/*Returns the texture ID.*/
	unsigned int GetID() { return m_ID; }
	/*Returns the textures height.*/
	unsigned int GetHeight() { return m_Height; }
	/*Returns the textures Width.*/
	unsigned int GetWidth() { return m_Width; }
	/*Returns the textures path when it was created.*/
	std::string GetPath() { return m_Path; }

protected:
	std::string m_Path;
	unsigned int m_ID;
	unsigned int m_Width;
	unsigned int m_Height;
	int m_ImageFormat;

	int m_WrapTParam, m_WrapSParam, m_MinFilterParam, m_MagFilterParam;
	unsigned int m_ActiveTextureUnit;
};