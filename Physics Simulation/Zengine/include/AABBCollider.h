#pragma once
#include "Collider.h"

class AABBCollider : public Collider
{
public:
	AABBCollider(GameObject* a_parent, std::string a_name);
	~AABBCollider();

	/*Fits the box around the positions.*/
	void Fit(std::vector<glm::vec3>& a_points);

	void ResetInertia();

	/*Manually set the size of the box.*/
	void SetSize(glm::vec3& a_value);

	glm::vec3 GetSize() { return m_Size; }
	glm::vec3 GetMin();
	glm::vec3 GetMax();

protected:
	glm::vec3 m_Size;
};