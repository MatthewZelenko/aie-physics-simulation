#pragma once
#include "Component.h"
class RigidBody;

class SpringJoint : public Component
{
public:
	SpringJoint();
	SpringJoint(GameObject* a_parent, std::string a_name);
	~SpringJoint();

	void Update(float a_deltaTime);

	void SetEndPoint(RigidBody* a_endPoint);
	void SetDistance(float a_distance);
	void SetDamp(float a_damp);
	void SetStiffness(float a_stiffness);

	RigidBody* GetEndPoint() { return m_EndPoint; }
	float GetDistance() { return m_Distance; }
	float GetDamp() { return m_Damp; }
	float GetStiffness() { return m_Stiffness; }

protected:
	RigidBody* m_EndPoint;
	float m_Distance;
	float m_Damp;
	float m_Stiffness;
};