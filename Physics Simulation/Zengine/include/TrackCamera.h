#pragma once
#include "Camera.h"
class TrackCamera :
	public Camera
{
public:
	TrackCamera();
	TrackCamera(GLFWwindow* a_window, GameObject* a_object, glm::vec3& a_displacement);
	~TrackCamera();

	void Update(float a_deltaTime);
	/*Sets the movement keys.*/
	//void SetInputKeys(int a_controlEnableKey);

	void SetTrackingObject(GameObject* a_object) { m_object = a_object; }
	void SetDisplacement(glm::vec3& a_displacement) { m_Displacement = a_displacement; }

	GameObject* GetObject() { return m_object; }
	glm::vec3 GetDisplacement() { return m_Displacement; }

protected:
	GameObject* m_object;
	glm::vec3 m_Displacement;
};