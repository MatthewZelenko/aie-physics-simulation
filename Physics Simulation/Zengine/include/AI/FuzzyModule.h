#pragma once
#include "FuzzySet.h"
#include "FuzzyRule.h"
#include <map>
#include <vector>

namespace Fuzzy
{
	class Member;
	class Term;
	enum DefuzzifyType
	{
		MAXAV,
		CENTROID
	};
	class Module
	{
	public:
		Module();
		~Module();
		void Destroy();

		/*Returns a set with the name*/
		Set& operator[](std::string a_name);

		/*Creates a set and returns a reference.
		Use a reference variable to hold the set.*/
		Set& CreateSet(std::string a_name);
		/*Adds a rule.*/
		void AddRule(Term* a_antecedent, Term* a_consequence);
		/*Returns a crisp value*/
		float Defuzzify(DefuzzifyType a_type, Set& a_input);
	private:
		void ZeroConsequences();
		std::vector<Rule> m_Rules;
		std::map<std::string, Set>m_Sets;
	};
}