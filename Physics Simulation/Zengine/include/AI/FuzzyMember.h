#pragma once
#include "FuzzyTerm.h"
#include <string>
namespace Fuzzy
{
	class Module;
	enum MemberType
	{
		TRIANGLE,
		LEFTSHOULDER,
		RIGHTSHOUDLER,
		TOTALTYPES
	};
#pragma region Member
	class Member : public Term
	{
	public:
		friend Module;
		Member(MemberType a_type, std::string a_name, float a_maxDOM, float a_startX, float a_endX);
		virtual ~Member();

		virtual float CalculateDOM(float a_Value) = 0;

		float ORWithDOM(float a_value);

		std::string GetName() { return m_Name; }
		MemberType GetType() { return m_Type; }
		float GetMaxDOM() { return m_MaxDOM; }

	protected:
		std::string m_Name;
		MemberType m_Type;
		float m_MaxDOM;
		float m_StartX, m_EndX;
	};
#pragma endregion Member
#pragma region Triangle
	class Triangle : public Member
	{
	public:
		Triangle(std::string a_name, float a_x0, float a_x1, float a_x2);
		~Triangle();

		float CalculateDOM(float a_Value);
	private:
		float m_X0;
	};
#pragma endregion Triangle
#pragma region LeftShoulder
	class LeftShoulder : public Member
	{
	public:
		LeftShoulder(std::string a_name, float a_X0, float a_X1);
		~LeftShoulder();

		float CalculateDOM(float a_Value);
	private:
		float m_X0;
	};
#pragma endregion LeftShoulder
#pragma region RightShoulder
	class RightShoulder : public Member
	{
	public:
		RightShoulder(std::string a_name, float a_x0, float a_x1, float a_x2);
		~RightShoulder();

		float CalculateDOM(float a_Value);
	private:
		float m_X0;
	};
#pragma endregion RightShoulder
}