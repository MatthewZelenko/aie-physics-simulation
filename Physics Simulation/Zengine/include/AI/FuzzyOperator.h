#pragma once
#include "FuzzyTerm.h"

namespace Fuzzy
{
	class Or;
	class Rule;

	class And : public Term
	{
	public:
		friend class Or;
		friend class Rule;

		And(Term* a_t0, Term* a_t1);
		~And();
		void Destroy();

		float GetDOM();
		float ORWithDOM(float a_value);
	private:
		Term* m_Terms[2];
	};

	class Or : public Term
	{
	public:
		friend class And;
		friend class Rule;
		Or(Term* a_t0, Term* a_t1);
		~Or();
		void Destroy();

		float GetDOM();
		float ORWithDOM(float a_value);
	private:
		Term* m_Terms[2];
	};
}