#pragma once
#include <GLM\glm.hpp>
#include <string>
#include <map>
#include "Component.h"
#include "Collider.h"
#include "SphereCollider.h"
#include "AABBCollider.h"
#include "RigidBody.h"
#include "Transform.h"
#include "SpringJoint.h"

class GameObject
{
public:
	GameObject(std::string a_name);
	GameObject(std::string a_name, glm::vec3& a_position, glm::vec3& a_size);
	virtual ~GameObject();

	void ClearAllComponents();

	virtual void ProcessInput(float a_deltaTime);
	virtual void Update(float a_deltaTime);

	//Collision functions
	virtual void OnCollisionEnter(GameObject* a_collider);
	virtual void OnCollisionStay(GameObject* a_collider);
	virtual void OnCollisionExit(GameObject* a_collider);
	
	//Component functions
	//Create functions
	template<class ComponentType>
	ComponentType& CreateComponent(std::string a_name);
	template<>
	Transform& CreateComponent(std::string a_name);
	template<>
	RigidBody& CreateComponent(std::string a_name);
	template<>
	SphereCollider& CreateComponent(std::string a_name);
	template<>
	AABBCollider& CreateComponent(std::string a_name);	
	template<>
	SpringJoint& CreateComponent(std::string a_name);
	//Remove functions
	template<class ComponentType>
	void RemoveComponent(std::string a_name);
	template<>
	void RemoveComponent<Transform>(std::string a_name);
	template<>
	void RemoveComponent<RigidBody>(std::string a_name);
	template<>
	void RemoveComponent<Collider>(std::string a_name);
	template<>
	void RemoveComponent<SphereCollider>(std::string a_name);
	template<>
	void RemoveComponent<AABBCollider>(std::string a_name);
	template<>
	void RemoveComponent<SpringJoint>(std::string a_name);
	//Clear
	template<class ComponentType>
	void ClearComponents();
	template<>
	void ClearComponents<Transform>();
	template<>
	void ClearComponents<RigidBody>();
	template<>
	void ClearComponents<Collider>();
	template<>
	void ClearComponents<SpringJoint>();
	//GetComponent
	template<class ComponentType>
	ComponentType* GetComponent();
	template<>
	Transform* GetComponent();
	template<>
	RigidBody* GetComponent();
	template<>
	Collider* GetComponent();
	template<>
	AABBCollider* GetComponent();
	template<>
	SphereCollider* GetComponent();
	template<>
	SpringJoint* GetComponent();

	template<class ComponentType>
	ComponentType* GetComponentByName(std::string a_name);
	template<>
	Transform* GetComponentByName(std::string a_name);
	template<>
	RigidBody* GetComponentByName(std::string a_name);
	template<>
	Collider* GetComponentByName(std::string a_name);
	template<>
	AABBCollider* GetComponentByName(std::string a_name);
	template<>
	SphereCollider* GetComponentByName(std::string a_name);
	template<>
	SpringJoint* GetComponentByName(std::string a_name);

	template<class ComponentType>
	std::map<std::string, ComponentType>* GetComponents();
	template<>
	std::map<std::string, Transform>* GetComponents();
	template<>
	std::map<std::string, RigidBody>* GetComponents();
	//template<>
	//std::map<std::string, Collider>* GetComponents();
	template<>
	std::map<std::string, SpringJoint>* GetComponents();

	//Getters
	std::string GetName() { return m_Name; }

protected:
	std::map<std::string, Transform> m_Transforms;
	std::map<std::string, RigidBody> m_RigidBodies;
	std::map<std::string, Collider*> m_Colliders;
	std::map<std::string, SpringJoint> m_Springs;

	std::string m_Name;
};

#pragma region CreateComponent
template<class ComponentType>
ComponentType& GameObject::CreateComponent(std::string a_name)
{
	return ComponentType(nullptr, "", "");
}
template<>
Transform& GameObject::CreateComponent(std::string a_name)
{
	if (m_Transforms.find(a_name) != m_Transforms.end())
	{
		m_Transforms.erase(a_name);
	}
	return m_Transforms[a_name] = Transform(this, a_name);
}
template<>
RigidBody& GameObject::CreateComponent(std::string a_name)
{
	if (m_RigidBodies.find(a_name) != m_RigidBodies.end())
	{
		m_RigidBodies.erase(a_name);
	}
	return m_RigidBodies[a_name] = RigidBody(this, a_name);
}
template<>
SphereCollider& GameObject::CreateComponent(std::string a_name)
{
	if (m_Colliders.find(a_name) != m_Colliders.end())
	{
		delete m_Colliders[a_name];
		m_Colliders.erase(a_name);
	}

	m_Colliders[a_name] = new SphereCollider(this, a_name);
	return *(SphereCollider*)m_Colliders[a_name];
}
template<>
AABBCollider& GameObject::CreateComponent(std::string a_name)
{
	if (m_Colliders.find(a_name) != m_Colliders.end())
	{
		delete m_Colliders[a_name];
		m_Colliders.erase(a_name);
	}

	m_Colliders[a_name] = new AABBCollider(this, a_name);
	return *(AABBCollider*)m_Colliders[a_name];
}
template<>
SpringJoint& GameObject::CreateComponent(std::string a_name)
{
	if (m_Springs.find(a_name) != m_Springs.end())
	{
		m_Springs.erase(a_name);
	}
	return m_Springs[a_name] = SpringJoint(this, a_name);
}
#pragma endregion CreateComponent
#pragma region RemoveComponent
template<class ComponentType>
void GameObject::RemoveComponent(std::string a_name)
{
	
}
template<>
void GameObject::RemoveComponent<Transform>(std::string a_name)
{
	if (m_Transforms.find(a_name) != m_Transforms.end())
	{
		m_Transforms.erase(a_name);
	}
}
template<>
void GameObject::RemoveComponent<RigidBody>(std::string a_name)
{
	if (m_RigidBodies.find(a_name) != m_RigidBodies.end())
	{
		m_RigidBodies.erase(a_name);
	}
}
template<>
void GameObject::RemoveComponent<Collider>(std::string a_name)
{
	if (m_Colliders.find(a_name) != m_Colliders.end())
	{
		delete m_Colliders[a_name];
		m_Colliders.erase(a_name);
	}
}
template<>
void GameObject::RemoveComponent<AABBCollider>(std::string a_name)
{
	if (m_Colliders.find(a_name) != m_Colliders.end())
	{
		if (m_Colliders[a_name]->GetColliderType() == "Box")
		{
			delete m_Colliders[a_name];
			m_Colliders.erase(a_name);
		}
	}
}
template<>
void GameObject::RemoveComponent<SphereCollider>(std::string a_name)
{
	if (m_Colliders.find(a_name) != m_Colliders.end())
	{
		if (m_Colliders[a_name]->GetColliderType() == "Sphere")
		{
			delete m_Colliders[a_name];
			m_Colliders.erase(a_name);
		}
	}
}
template<>
void GameObject::RemoveComponent<SpringJoint>(std::string a_name)
{
	if (m_Springs.find(a_name) != m_Springs.end())
	{
		m_Springs.erase(a_name);
	}
}
#pragma endregion RemoveComponent
#pragma region ClearComponents
template<class ComponentType>
void GameObject::ClearComponents()
{
	
}
template<>
void GameObject::ClearComponents<Transform>()
{
	m_Transforms.clear();
}
template<>
void GameObject::ClearComponents<RigidBody>()
{
	m_RigidBodies.clear();
}
template<>
void GameObject::ClearComponents<Collider>()
{
	for (std::map<std::string, Collider*>::iterator iter = m_Colliders.begin(); iter != m_Colliders.end(); ++iter)
	{
		delete (*iter).second;
	}
	m_Colliders.clear();
}
template<>
void GameObject::ClearComponents<SpringJoint>()
{
	m_Springs.clear();
}
#pragma endregion ClearComponents
#pragma region GetComponent
template<class ComponentType>
ComponentType* GameObject::GetComponent()
{
	return Collider(nullptr, "", "");
}
template<>
Transform* GameObject::GetComponent()
{
	if (!m_Transforms.empty())
	{
		return &(*m_Transforms.begin()).second;
	}
	return nullptr;
}
template<>
RigidBody* GameObject::GetComponent()
{
	if (!m_RigidBodies.empty())
	{
		return &(*m_RigidBodies.begin()).second;
	}
	return nullptr;
}
template<>
Collider* GameObject::GetComponent()
{
	if (!m_Colliders.empty())
		return (*m_Colliders.begin()).second;
	return nullptr;
}
template<>
SphereCollider* GameObject::GetComponent()
{
	for(auto& iter: m_Colliders)
	{
		if(iter.second->GetColliderType() == "Sphere")
			return (SphereCollider*)iter.second;
	}
	return nullptr;
}
template<>
AABBCollider* GameObject::GetComponent()
{
	for (auto& iter : m_Colliders)
	{
		if (iter.second->GetColliderType() == "AABB")
			return (AABBCollider*)iter.second;
	}
	return nullptr;
}
template<>
SpringJoint* GameObject::GetComponent()
{
	if (!m_Springs.empty())
	{
		return &(*m_Springs.begin()).second;
	}
	return nullptr;
}
#pragma endregion GetComponent
#pragma region GetComponentByName
template<class ComponentType>
ComponentType* GameObject::GetComponentByName(std::string a_name)
{
	return Collider(nullptr, "", "");
}
template<>
Transform* GameObject::GetComponentByName(std::string a_name)
{
	if (m_Transforms.find(a_name) != m_Transforms.end())
		return &m_Transforms[a_name];
	return nullptr;
}
template<>
RigidBody* GameObject::GetComponentByName(std::string a_name)
{
	if (m_RigidBodies.find(a_name) != m_RigidBodies.end())
		return &m_RigidBodies[a_name];
	return nullptr;
}
template<>
Collider* GameObject::GetComponentByName(std::string a_name)
{
	if (m_Colliders.find(a_name) != m_Colliders.end())
		return m_Colliders[a_name];
	return nullptr;
}
template<>
SphereCollider* GameObject::GetComponentByName(std::string a_name)
{
	if (m_Colliders.find(a_name) != m_Colliders.end())
		if (m_Colliders[a_name]->GetColliderType() == "Sphere")
		{
			return (SphereCollider*)m_Colliders[a_name];
		}
	return nullptr;
}
template<>
AABBCollider* GameObject::GetComponentByName(std::string a_name)
{
	if (m_Colliders.find(a_name) != m_Colliders.end())
		if (m_Colliders[a_name]->GetColliderType() == "Box")
		{
			return (AABBCollider*)m_Colliders[a_name];
		}
	return nullptr;
}
template<>
SpringJoint* GameObject::GetComponentByName(std::string a_name)
{
	if (m_Springs.find(a_name) != m_Springs.end())
		return &m_Springs[a_name];
	return nullptr;
}
#pragma endregion GetComponentByName
#pragma region GetComponents
template<class ComponentType>
std::map<std::string, ComponentType>* GameObject::GetComponents()
{
	return nullptr;
}
template<>
std::map<std::string, Transform>* GameObject::GetComponents<Transform>()
{
	return &m_Transforms;
}
template<>
std::map<std::string, RigidBody>* GameObject::GetComponents<RigidBody>()
{
	return &m_RigidBodies;
}
//template<>
//std::map<std::string, Collider*>* GameObject::GetComponents<Collider>()
//{
//	return &m_Colliders;
//}
template<>
std::map<std::string, SpringJoint>* GameObject::GetComponents<SpringJoint>()
{
	return &m_Springs;
}
#pragma endregion GetComponents