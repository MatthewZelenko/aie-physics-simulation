#pragma once
#include "Collider.h"

class SphereCollider : public Collider
{
public:
	SphereCollider(GameObject* a_parent, std::string a_name);
	~SphereCollider();

	/*Fits the sphere around the points.*/
	void Fit(std::vector<glm::vec3>& a_points);

	void ResetInertia();

	/*Sets the radius of the sphere.*/
	void SetRadius(float a_value);
	/*Multiplies the radius by set amount.*/
	void ScaleRadius(float a_scale);

	/*Returns the radius of the sphere.*/
	float GetRadius() { return m_Radius; }


private:
	float m_Radius;
};