#pragma once
#include "gl_core_4_4.h"
#include "GLFW\glfw3.h"
#include <string>
#include "Debugger.h"
#include "Scene.h"

class Application
{
public:
	Application(std::string	a_gameTitle, unsigned int a_gameWidth, unsigned int a_gameHeight);
	virtual ~Application();

	/*Call this function in main loop only.
	Will run ProcessInput(), Update(), Render() in a loop
	and run Load() and UnLoad() once.*/
	void Run();

	GLFWwindow& GetWindow() { return *m_GameWindow; }
	unsigned int GetWidth() { return m_GameWidth; }
	unsigned int GetHeight() { return m_GameHeight; }
	//Returns the time change between frames.
	double GetDeltaTime() { return m_DeltaTime; }
	//Return current running game time.
	double GetRunTime() { return m_RunTime; }

	Scene* AddScene(Scene* scene, std::string name);
	void RemoveScene(std::string name);
	void PushScene(std::string name);
	void PushScene(Scene* scene);
	void PopScene();
	void PopToScene(std::string name);
	void PopAllScenes();
	void ChangeScene(std::string a_name);

	Scene* GetScene(std::string name) { return m_Scenes[name]; }
	Scene* GetCurrentScene() { return m_CurrentScenes.back(); }
	Scene* GetUnderStackScene() { return m_CurrentScenes.at(m_CurrentScenes.size() - 2); }

	void SetTimeStep(float a_timeStep);
	float GetTimeStep() { return m_TimeStep; }
	float GetElapsedTimeStep() { return m_ElapsedTimeStep; }

protected:
	//Runs once at the start.
	virtual void Load() = 0;
	//The input loop.
	virtual void ProcessInput() = 0;
	//Main game loop.
	virtual void Update() = 0;
	//The rendering loop.
	virtual void Render() = 0;
	//Runs once upon shutdown.
	virtual void UnLoad() = 0;
	
	bool m_GameIsRunning;
	GLFWwindow* m_GameWindow;
	unsigned int m_GameWidth, m_GameHeight;

	std::string	m_GameTitle;
	double m_DeltaTime;
	double m_RunTime;

	std::map<std::string, Scene*> m_Scenes;
	std::vector<Scene*> m_CurrentScenes;
	bool m_PushedOnStack;

private:
	//For initalizing glfw, glew, etc.
	void SystemStartup();
	//For updating deltaTime, etc.
	void SystemUpdate();
	//CleanUp function on exit.
	void SystemShutDown();

	//Runs every frame
	void SceneLoad();
	void SceneProcessInput();
	void SceneUpdate();
	void SceneRender();
	void SceneUnLoad();

	float m_ElapsedTimeStep;
	float m_TimeStep;
};