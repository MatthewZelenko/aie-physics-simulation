#pragma once
#include <string>
#include "Component.h"
#include <GLM\glm.hpp>
#include <vector>

enum class Combine
{
	MINIMUM,
	AVERAGE,
	MULTIPLY,
	MAXIMUM
};

class Collider : public Component
{
public:

	Collider(GameObject* a_parent, std::string a_name, std::string a_type);
	virtual ~Collider();

	/*Takes in positions and creates a bounding object surrounding all of them.*/
	virtual void Fit(std::vector<glm::vec3>& a_points) = 0;

	/*Manually set the position of the box.*/
	void SetPosition(glm::vec3& a_position);
	void SetStaticFriction(float a_friction);
	void SetDynamicFriction(float a_friction);

	//Default Restitution = 1.0f
	void SetRestitution(float a_restitution);
	void SetCombine(Combine a_combine);

	/*Moves the box by set amount.*/
	void AddPosition(glm::vec3& a_position);
	void AddStaticFriction(float a_friction);
	void AddDynamicFriction(float a_friction);
	void AddRestitution(float a_restitution);

	std::string GetColliderType() { return m_Type; }
	glm::vec3 GetPosition();
	glm::vec3 GetLocalPosition() { return m_Position; }

	/*No In Use TODO:*/
	float GetStaticFriction() { return m_StaticFriction; }
	float GetDynamicFriction() { return m_DynamicFriction; }
	
	float GetRestitution() { return m_Restitution; }
	Combine GetCombine() { return m_Combine; }
	glm::mat3 GetInertia() { return m_Inertia; }
	glm::mat3 GetInverseInertia() { return m_InverseInertia; }

	virtual void ResetInertia() = 0;

protected:
	std::string m_Type;
	glm::vec3 m_Position;

	/*No In Use TODO:*/
	float m_StaticFriction;
	float m_DynamicFriction;
	
	float m_Restitution;
	glm::mat3 m_Inertia;
	glm::mat3 m_InverseInertia;
	Combine m_Combine;
};