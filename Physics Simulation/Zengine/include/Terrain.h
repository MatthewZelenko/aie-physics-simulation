#pragma once
#include "Shader.h"
#include <vector>
class Terrain
{
public:
	Terrain();
	~Terrain();
	/*Destroys and clears everything.*/
	void Destroy();

	/*Generates base terrain grid and creates all buffers*/
	void Init(unsigned int a_rows, unsigned int a_cols, float a_width, float a_height);
	/*Generates land terrain.*/
	void GenerateLand();

	/*Draws land terrain.*/
	void DrawLand(Shader a_shader);

	/*Sets land amplitude.*/
	void SetLandAmplitude(float a_amplitude);
	/*Sets land persistence.*/
	void SetLandPersistence(float a_persistence);
	/*Sets land frequency.*/
	void SetLandFrequency(float a_frequency);
	/*Sets lands perlin ocataves.*/
	void SetLandOctave(unsigned int a_octave);
	/*Sets land frequency.*/
	void SetLandSeed(float a_seed);

	/*Adds land amplitude.*/
	void AddLandAmplitude(float a_amplitude);
	/*Adds land persistence.*/
	void AddLandPersistence(float a_persistence);
	/*Adds land frequency.*/
	void AddLandFrequency(float a_frequency);
	/*Adds lands perlin ocataves.*/
	void AddLandOctave(unsigned int a_octave);
	/*Adds land frequency.*/
	void AddLandSeed(float a_seed);

	/*Returns the height at a given position*/
	float GetLandHeightFromPosition(float a_x, float a_z, bool precision = false);
	/*Returns the lands at a given position
	TODO:*/
	glm::vec3 GetLandPositionFromPosition(int a_x, int a_z);
	/*Returns the normal at a given position*/
	glm::vec3 GetLandNormalFromPosition(int a_x, int a_z, bool precision = false);

	glm::vec3 GetLandFromPosition(int a_x, int a_z);

	/*Returns the height at a given index
	TODO:*/
	float GetLandHeightFromIndex(int a_x, int a_z);
	/*Returns the lands at a given index
	TODO:*/
	glm::vec3 GetLandPositionFromIndex(int a_x, int a_z);
	/*Returns the normal at a given index
	TODO:*/
	glm::vec3 GetLandNormalFromIndex(int a_x, int a_z);

protected:
	void GenerateGrid();
	void CreateBuffers();

	unsigned int m_Rows, m_Cols;
	float m_Width, m_Height;
	Shader m_UpdateShader;

	struct Vertex
	{
		glm::vec3 Position;
		glm::vec3 Normal;
		glm::vec2 TexCoord;
	};

	//Land
	unsigned int m_LandVAO[2], m_LandVBO[2];
	unsigned int m_LandActiveBuffer;
	std::vector<Vertex> m_LandVertices;
	float m_LandAmplitude, m_LandFrequency, m_LandPersistence;
	unsigned int m_LandOctaves;
	float m_LandSeed;
	bool m_LandIsDirty;
};